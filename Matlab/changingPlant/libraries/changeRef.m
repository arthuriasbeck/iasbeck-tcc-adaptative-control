function refSigChange = changeRef(loops, tInput, T, refSig)
    refSigChange = refSig;
    if ~mod(loops - 1,round(tInput/T))
        if refSig == 1
            refSigChange = -1;
        else if refSig == -1
            refSigChange = 1;
            end
        end
    end
end