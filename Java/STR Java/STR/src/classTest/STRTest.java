package classTest;

import control.STR;

/**
 * Esta classe � uma tradu��o direta dos c�digos em Matlab que implementam o
 * controlador STR. Aqui � utilizada a classe STR, que possui um m�todo
 * (compute) respons�vel por realizar a estima��o e a a��o de controle.
 * 
 * @author Arthur Iasbeck
 *
 */

public class STRTest {
	public static void main(String args[]) {
		// Este programa tem como finalidade implementar o controlador desenvolvido
		// atrav�s do m�todo pole placement, que depende dos par�metros da planta.
		// Aqui, valores de sa�da que na implementa��o final ser�o obtidos do rob�,
		// ser�o obtidos atrav�s de uma equa��o diferen�a (que foi gerada atrav�s de
		// uma estima��o experimental). Com os valores de sa�da, o m�todo dos
		// m�nimos quadrados tentar� estimar os par�metros da planta e atrav�s da
		// utiliza��o destes par�metros, ser� obtida a a��o de controle u.

		// Par�metros para execu��o
		double T = 0.05; // Tempo de amostragem
		double tSim = 10; // Tempo de simula��o
		double tInput = 3; // Tempo para que ocorra uma mudan�a na entrada do sistema
		double ucAmp = 90; // Determina a amplitude da entrada degrau
		double ucSig = 1; // Determina se o degrau ser� positivo ou negativo

		// Par�metros do modelo
		double am1 = -1.3205;
		double am2 = 0.4966;

		// Inicializa��o de vari�veis utilizadas na simula��o da planta
		double y = 0; // y(t)
		double y_1 = 0; // y(t-1)
		double y_2 = 0; // y(t-2)
		double u = 0; // u(t)
		double u_1 = 0; // u(t-1)
		double u_2 = 0; // u(t-2)

		// Inicializa��o das vari�veis utilizadas na simula��o do modelo
		double ym = 0; // ym(t)
		double uc = 0; // uc(t)

		STR strControl = new STR(am1, am2);

		// Vari�veis para controle de execu��o
		int numLoops = (int) Math.round(tSim / T);

		for (int loops = 0; loops < numLoops; loops++) {
			// Computando sa�da da planta
			y = 1.0002 * y_1 + 0.0008 * y_2 - 0.0244 * u_1 + 0.1503 * u_2;

			// Computando as entradas
			if (loops % (tInput / T) == 0) {
				if (ucSig == 1) {
					ucSig = -1;
				} else if (ucSig == -1) {
					ucSig = 1;
				}
			}
			uc = ucAmp * ucSig;

			u = strControl.compute(y, uc);
			ym = strControl.getModelOutput();

			y_2 = y_1;
			y_1 = y;
			u_2 = u_1;
			u_1 = u;

			System.out.println(y + "\t" + ym + "\t" + uc + "\t" + u);
		}
	}
}