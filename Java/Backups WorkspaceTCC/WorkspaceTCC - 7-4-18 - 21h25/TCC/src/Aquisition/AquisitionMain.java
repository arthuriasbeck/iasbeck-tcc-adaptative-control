package Aquisition;

import java.util.ArrayList;

import Control.PID;
import File.FileHandler;
import Sensors.RGB;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.nxt.SensorPort;
import lejos.util.Delay;

public class AquisitionMain {

	public static final int REF = 300;
	public static final double KP = 0.5;
	public static final double KI = 0;
	public static final double KD = 0;

	public static void main(String[] args) {
		NXTMotor motorCte = new NXTMotor(MotorPort.A); // Objeto para o motor de vel cte
		NXTMotor motorControl = new NXTMotor(MotorPort.B); // Objeto para o motor de vel vari�vel
		RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		ArrayList<Double> uData = new ArrayList<Double>(); // ArrayList de entradas
		ArrayList<Double> yData = new ArrayList<Double>(); // ArrayList de sa�das
		FileHandler fileU = new FileHandler("u.dat"); // Arquivo para as entradas
		FileHandler fileY = new FileHandler("y.dat"); // Arquivo para as sa�das
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double u; // A��o de controle

		// Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		// pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		// motores) seja analisada separadamente.
		motorCte.setPower(50);
		while (Button.readButtons() == 0) {
			// rbgValue = rgb.getSensor();
			rbgValue = Math.random() * 20;
			u = pid.compute(rbgValue);
			motorControl.setPower((int) Math.round(u));
			uData.add(u);
			yData.add(rbgValue);
			LCD.clear();
			LCD.drawString("u = " + u, 0, 0);
			LCD.drawString("y = " + rbgValue, 0, 1);
			Delay.msDelay(100);
		}
		motorCte.stop();
		motorControl.stop();
		fileU.saveInFile(uData);
		fileY.saveInFile(yData);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
