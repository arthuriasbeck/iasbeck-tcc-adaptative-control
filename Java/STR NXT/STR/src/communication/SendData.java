package communication;

import java.util.ArrayList;

import lejos.nxt.Button;
import lejos.nxt.LCD;

public class SendData {
	public boolean debug = false;
	String[] dataIndex = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
			"s", "t", "u", "v", "w", "x", "y", "z" };
	USBComm usbComm;

	public SendData() {
		usbComm = new USBComm();
		if (!debug) {
			usbComm.connect();
		}
	}

	public void send(ArrayList<Double> msg) {
		int msgSize = msg.size();
		double[] msgArray = new double[msgSize];
		for (int i = 0; i < msgSize; i++) {
			msgArray[i] = msg.get(i);
		}
		send(msgArray);
	}
	
	public void send(double[] msg) {
		String finalMsg = "";
		for (int i = 0; i < msg.length; i++) {
			if (i == msg.length - 1) {
				finalMsg += msg[i]; // Ao fim da mensagem n�o � acrescentada uma letra de �ndice
			} else {
				finalMsg += msg[i] + dataIndex[i];
			}

		}
		if (debug) {
			LCD.clear();
			LCD.drawString(finalMsg, 0, 0);
		} else {
			usbComm.send(finalMsg);
		}
	}
	
	public void end() {
		usbComm.send("*");
	}

	public static void main(String args[]) {
		SendData sendData = new SendData();
		double[] msg = { 4.56, 1, 2, 3 };
		sendData.send(msg);
		
		for(int i = 0; i < 40; i++) {
			msg[0] = i;
			msg[1] = i+1;
			msg[2] = i+2;
			msg[3] = i+3;
			sendData.send(msg);
		}
		sendData.end();
		LCD.drawString("Fim da execucao", 0, 0);
		Button.waitForAnyPress();
	}
}
