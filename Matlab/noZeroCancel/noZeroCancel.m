% Este c�digo mostra a implementa��o de um STR que n�o cancela os zeros da
% planta do processo controlado.

clear; clc; close all;
addpath('../libraries/PlotData');
addpath('libraries');
% Par�metros de execu��o
% T = 0.05;
T = 0.5;
tSim = 15;

% Par�metros da planta
% a1 = -1.0002;
% a2 = -0.0008;
% b0 = -0.0244;
% b1 = 0.1503;
a1 = -1.6065;
a2 = 0.6065;
b0 = 0.1065;
b1 = 0.0902;

% Par�metros do modelo 
am1 = -1.3205;
am2 = 0.4966;
a0 = 0;

% Vari�veis para execu��o
% Sa�das para a planta 
y = 0;
y_1 = 0;
y_2 = 0;
% Entradas para a planta 
u = 0;
u_1 = 0;
u_2 = 0;
% Sa�das para o modelo 
ym = 0;
ym_1 = 0;
ym_2 = 0;
% Entradas para o modelo
uc = 0;
uc_1 = 0;
uc_2 = 0;

uc = 1;
yArray = zeros(1,1);
ymArray = zeros(1,1);
uArray = zeros(1,1);
loops = tSim/T;

for i = 1:loops
    % Computando a��o de controle e sa�das da planta e do modelo
    [s0, s1, r1, beta] = getControlPar(a0, a1, a2, b0, b1, am1, am2);
    y = -a1*y_1 - a2*y_2 + b0*u_1 + b1*u_2;
    ym = -am1*ym_1 - am2*ym_2 + beta*b0*uc_1 + beta*b1*uc_2;
    u = -r1*u_1 + beta*uc + beta*a0*uc_1 - s0*y - s1*y_1;
      
    % Armaenando informa��es sobre execu��o
    yArray(i) = y;
    ymArray(i) = ym;
    uArray(i) = u;
    
    % Atualilzando vari�veis que armazenam entradas e sa�das anteriores
    uc_2 = uc_1;
    uc_1 = uc;
    ym_2 = ym_1;
    ym_1 = ym;
    u_2 = u_1;
    u_1 = u;
    y_2 = y_1;
    y_1 = y;
end

% Motrando graficamente os resultados 
time = (0:i-1)*T;
x = 'Tempo(s)';
y = 'Sa�da';
title = 'Respostas do modelo e do sistema controlado';
f = 'zeroCancelOutput';
completePlotDigital(1,time,yArray,'bo',1.5,x,y,title,'grid',0,1,'','',0);
completePlotDigital(1,time,ymArray,'r',1.5,x,y,title,'grid',0,1,'','',1);
lgd = legend('Sa�da real','Sa�da desejada');
lgd.Position(2) = lgd.Position(2) - 0.1;
completePlotDigital(1,time,ymArray,'r',1.5,x,y,title,'grid',0,1,f,'',1);

y = 'Entrada';
title = 'Entrada';
f = 'zeroCancelInput';
completePlotDigital(2,time,uArray,'b',1.5,x,y,title,'grid',0,1,f,'',1);