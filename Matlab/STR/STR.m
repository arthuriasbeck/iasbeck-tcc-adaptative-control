% Este programa tem como finalidade implementar o controlador desenvolvido
% atrav�s do m�todo pole placement, que depende dos par�metros da planta.
% Aqui, valores de sa�da que na implementa��o final ser�o obtidos do rob�,
% ser�o obtidos atrav�s de uma equa��o diferen�a (que foi gerada atrav�s de
% uma estima��o experimental). Com os valores de sa�da, o m�todo dos
% m�nimos quadrados tentar� estimar os par�metros da planta e atrav�s da
% utiliza��o destes par�metros, ser� obtida a a��o de controle u.

addpath('libraries');
addpath('../libraries/PlotData');

clear; clc; close all;

% Par�metros para execu��o
T = 0.075;            % Tempo de amostragem
tSim = 15.98;         % Tempo de simula��o
tInput = 4;        % Tempo para que ocorra uma mudan�a na entrada do sistema 
ucAmp = 90;         % Determina a amplitude da entrada degrau
ucSig = -1;         % Determina se o degrau ser� positivo ou negativo

% Par�metros do modelo
ta = 1;
Mp = 5;
[am1 ,am2] = getModel(ta, Mp, T);
a0 = 0;

% Inicializa��o de vari�veis para armazenamento de dados
uArray = zeros(1,1);    % Vetor que armazena os valores de entrada
ucArray = zeros(1,1);   % Vetor que armazena os valores de refer�ncia
yArray = zeros(1,1);    % Vetor que armazena os valores de sa�da
yeArray = zeros(1,1);   % Vetor que armazena os valores de estima��o
ymArray = zeros(1,1);   % Vetor que armazena os valores do modelo
a1Array = zeros(1,1);   % Vetor que armazena os valores de a1
a2Array = zeros(1,1);   % Vetor que armazena os valores de a2
b0Array = zeros(1,1);   % Vetor que armazena os valores de b0
b1Array = zeros(1,1);   % Vetor que armazena os valores de b1

% Inicializa��o de vari�veis utilizadas na simula��o da planta
y = 0;          % y(t)
y_1 = 0;        % y(t-1)
y_2 = 0;        % y(t-2)
u = 0;          % u(t)
u_1 = 0;        % u(t-1)
u_2 = 0;        % u(t-2)
initialY = 0;   % Condi��o inicial para a sa�da do sistema

% Inicializa��o das vari�veis utilizadas na simula��o do modelo
ym = 0;     % ym(t)
ym_1 = 0;   % ym(t-1)
ym_2 = 0;   % ym(t-2)
uc = 0;     % uc(t)
uc_1 = 0;   % uc(t-1)
uc_2 = 0;   % uc(t-2)

% Inicializa��o das vari�veis utilizadas na estima��o
ye = 0;                     % Sa�da estimada pelo m�nimos quadrados
fi = [-y_1 -y_2 u_1 u_2];   % Vetor de vari�veis conhecidas
parNum = length(fi);        % Quantidade de par�metros a serem determinados
P = rand(parNum);           % Matriz de corre��o
teta = rand(parNum,1);      % Vetor de par�metros

% Inicializa��o dos par�metros da planta
a1 = 0;
a2 = 0;
b0 = 0;
b1 = 0;

% Vari�veis para controle de execu��o
numLoops = round(tSim/T);

for loops = 1:numLoops
    % Computando sa�da da planta
    if loops == 1
        y = initialY;
    else
        % Fun��o que representa o seguidor
        y = 1.0002*y_1 + 0.0008*y_2 - 0.0244*u_1 + 0.1503*u_2;
        
        % Fun��o que representa a planta G(s) = 1/s(s+1)
        % y = 1.6065*y_1 - 0.6065*y_2 + 0.1065*u_1 + 0.0902*u_2;
    end
    
    % Atualiza��o dos par�metros do modelo
    fi = [-y_1 -y_2 u_1 u_2]';
    P = P - P*fi*(1/(1 + fi'*P*fi))*fi'*P;
    K = P*fi;
    ye = fi'*teta;
    errEst = y - ye;
    teta = teta + K*errEst;
    
    % Atualizando par�metros da planta
    a1 = teta(1);
    a2 = teta(2);
    b0 = teta(3);
    b1 = teta(4);
    
    % Atualizando par�metros do controlador 
    [s0, s1, r1, beta] = getControlPar(a0, a1, a2, b0, b1, am1, am2);
    
    % Computando sa�da do modelo 
    ym = -am1*ym_1 - am2*ym_2 + beta*b0*uc_1 + beta*b1*uc_2;
    
    % Computando as entradas
    ucSig = changeRef(loops, tInput, T, ucSig);
    uc = ucAmp*ucSig;
    u = -r1*u_1 + beta*uc + beta*a0*uc_1 - s0*y - s1*y_1;
    
    % Descomente a linha abaixo caso deseje verificar a influ�ncia da
    % limita��o da sa�da
    % u = correctInput(u);
    
    % Armazenando dados de execu��o
    uArray(loops) = u;
    ucArray(loops) = uc;
    yArray(loops) = y;
    yeArray(loops) = ye;
    ymArray(loops) = ym;
    a1Array(loops) = a1;
    a2Array(loops) = a2;
    b0Array(loops) = b0;
    b1Array(loops) = b1;
    
    % Atualizando vari�veis que armanezam dados de itera��es passadas
    y_2 = y_1;
    y_1 = y;
    u_2 = u_1;
    u_1 = u;
    ym_2 = ym_1;
    ym_1 = ym;
    uc_2 = uc_1;
    uc_1 = uc;
end

% Mostrando os resultados da simula��o
U = uArray;
Uc = ucArray;
Y = yArray;
YE = yeArray;
YM = ymArray;
A1 = a1Array;
A2 = a2Array;
B0 = b0Array;
B1 = b1Array;

% Par�metros relacionados ao seguidor
TETA = [-1.0002 -0.0008 -0.0244 0.1503];

% Par�metros relacionados � planta G(s) = 1/s(s+1)
% TETA = [-1.6065 0.6065 0.1065 0.0902];

plotResultsControl(numLoops, T, U, Uc, Y, YE, YM, A1, A2, B0, B1, TETA);
