package Sensors;

import lejos.nxt.ColorSensor;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;

public class RGB {
	
	ColorSensor colorSensor;
	
	RGB(SensorPort sensorPort, int color){
		colorSensor = new ColorSensor(sensorPort, color);
	}
	
	public RGB(SensorPort sensorPort){
		colorSensor = new ColorSensor(sensorPort, 0);
	}
	
	public double getSensor() {
		return colorSensor.getRawLightValue();
	}
	
	public void calibrate() {
		LCD.clear();
		LCD.drawString("Sensor no branco", 0, 0);
		colorSensor.calibrateHigh();
		LCD.clear();
		LCD.drawString("Sensor no preto", 0, 0);
		colorSensor.calibrateLow();
		LCD.clear();
		LCD.drawString("Calibracao finalizada", 0, 0);
	}
	
	public static void main(String args[]) {
		double rgbValue;
		RGB rgbSensor = new RGB(SensorPort.S1);
		while(true) {
			rgbValue = rgbSensor.getSensor();
			LCD.drawString("RGB sensor = ", 0, 0);
			LCD.drawString("" + rgbValue, 0, 1);
		}
	}
}
