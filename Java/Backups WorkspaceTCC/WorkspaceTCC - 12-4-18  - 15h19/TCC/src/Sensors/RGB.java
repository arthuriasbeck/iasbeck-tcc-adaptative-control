package Sensors;

import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.util.Delay;

public class RGB {
	
	ColorSensor colorSensor;
	
	RGB(SensorPort sensorPort, int color){
		colorSensor = new ColorSensor(sensorPort, color);
	}
	
	public RGB(SensorPort sensorPort){
		colorSensor = new ColorSensor(sensorPort, 0);
	}
	
	public int getSensor() {
		return colorSensor.getRawLightValue();
	}
	
	public void calibrate() {
		LCD.clear();
		LCD.drawString("Sensor no branco", 0, 0);
		colorSensor.calibrateHigh();
		LCD.clear();
		LCD.drawString("Sensor no preto", 0, 0);
		colorSensor.calibrateLow();
		LCD.clear();
		LCD.drawString("Calibracao finalizada", 0, 0);
	}
	
	public static void main(String args[]) {
		int rgbValue = 0;
		RGB rgbSensor = new RGB(SensorPort.S1);
		int whiteValue; 
		int blackValue;
		while(Button.readButtons() == 0) {
			rgbValue = rgbSensor.getSensor();
			LCD.drawString("No branco:", 0, 0);
			LCD.drawString("RGB sensor = ", 0, 1);
			LCD.drawString("" + rgbValue, 0, 2);
			Delay.msDelay(150);
		}
		whiteValue = rgbValue;
		while(Button.readButtons() != 0);
		
		while(Button.readButtons() == 0) {
			rgbValue = rgbSensor.getSensor();
			LCD.drawString("No preto::", 0, 0);
			LCD.drawString("RGB sensor = ", 0, 1);
			LCD.drawString("" + rgbValue, 0, 2);
			Delay.msDelay(150);
		}
		blackValue = rgbValue;
		while(Button.readButtons() != 0);
		LCD.clear();
		LCD.drawString("Cinza claro=" + (0.6*whiteValue + 0.4*blackValue), 0, 0);
		LCD.drawString("Cinza=" + ((whiteValue + blackValue)/2), 0, 1);
		Button.waitForAnyPress();
	}
}
