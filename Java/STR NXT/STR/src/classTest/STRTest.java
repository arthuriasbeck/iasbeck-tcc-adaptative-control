package classTest;

import java.util.ArrayList;

import communication.SendData;
import control.STR;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import motors.NewMotor;
import sensors.Compass;

/**
 * Esta classe � uma tradu��o direta dos c�digos em Matlab que implementam o
 * controlador STR. Aqui � utilizada a classe STR, que possui um m�todo
 * (compute) respons�vel por realizar a estima��o e a a��o de controle.
 * 
 * @author Arthur Iasbeck
 *
 */

// TODO: Criar uma fun��o calibrate 

public class STRTest {

	// Constantes para controle de execu��o
	public static final boolean motor = true;
	public static final boolean usb = false;
	public static final boolean estimation = false;
	public static final long refTime = 3000;
	public static final long simTime = refTime*4;
	
	@SuppressWarnings("unused")
	public static void main(String args[]) {
		// Vari�veis para controle de execu��o
		double computeError;
		
		// Par�metros do modelo
		double ym;
		double Mp = 0.1;
		double ta = 1;
		double T = 0.05;

		// Inicializa��o de vari�veis utilizadas na simula��o da planta
		double y = 0;
		double u = 0;

		// Inicializa��o das vari�veis utilizadas no controle do sistema
		STR strControl = new STR(Mp, ta, T, estimation);
		strControl.load();
		Compass compass = new Compass(SensorPort.S2);
		NewMotor motorA = new NewMotor(MotorPort.A, true);
		NewMotor motorB = new NewMotor(MotorPort.B, true);
		double time;
		double sampleTime;
		double initialTime;
		double lastRefTime = 0;

		// Inicializa��o das vari�veis para envio dos dados da execu��o via USB
		SendData sendData = null;
		ArrayList<Double> msg = new ArrayList<Double>();
		if (usb) {
			sendData = new SendData();
		}

		// Aguardando entrada do usu�rio para executar
		LCD.clear();
		LCD.drawString("Press to start", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0);
		LCD.clear();
		
		// Inicializa��o das vari�veis utilizadas na simula��o do modelo
		double uc = 0;
		double ucMax = compass.getSensor() + 40;
		double ucMin = compass.getSensor() - 40;

		// La�o de execu��o
		time = System.currentTimeMillis();
		initialTime = System.currentTimeMillis();
		while (Button.readButtons() == 0 && (System.currentTimeMillis() - initialTime) < simTime) {
			// Computando sa�da da planta
			y = compass.getSensor();
			
			// Alterando a refer�ncia
			if(System.currentTimeMillis() - lastRefTime > refTime) {
				lastRefTime = System.currentTimeMillis();
				if(uc == 0) uc = ucMax;
				else if(uc == ucMax) uc = ucMin;
				else if(uc == ucMin) uc = ucMax;
			}
			
			// Computando a entrada e atuando no sistema
			u = strControl.compute(y, uc);
			if (motor) {
				motorA.setPower((int) Math.round(40 + u));
				motorB.setPower((int) Math.round(40 - u));
				motorA.forward();
				motorB.forward();
			}
			
			// Obtendo sa�da do modelo
			ym = strControl.getModelOutput();

			// Contagem do tempo de simula��o
			sampleTime = System.currentTimeMillis() - time;
			time = System.currentTimeMillis();

			// Enviando os dados da execu��o via USB
			if (usb) {
				msg.clear();
				msg.add(y);
				msg.add(uc);
				msg.add(ym);
				msg.add(u);
				msg.add(time);
				sendData.send(msg);
			}
		}

		// Finalizando execu��o
		motorA.stop();
		motorB.stop();
		strControl.save();
		while (Button.readButtons() != 0);
		if (usb) {
			sendData.end();
		}
		LCD.clear();
		LCD.drawString("Fim da execucao", 0, 0);
	}
}