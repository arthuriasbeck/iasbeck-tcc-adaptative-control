% Este c�digo mostra a implementa��o do M�todo dos M�nimos Quadrados a um
% conjunto de dados gerado por uma rela��o polinimial. O m�todo tenta
% ajustar um dado modelo ao conjunto de dados gerados. S�o aqui
% apresentados quatro modelos.

clear; clc; close all;

b0 = 1;
b1 = 0.45;
b2 = 0.1;

%-----------------------------------------------------------------------

U = 1:10;
Y = zeros(1,1);
FI = zeros(1,1);

for i = 1:10
    u = U(i);
    fiT = 1;
    FI(i,:) = fiT;
    y = b0 + b1*u + b2*u^2 + (rand/10);
    Y(i) = y;
end
Y = Y';

TETA0 = inv(FI'*FI)*FI'*Y;
Y0 = FI*TETA0;

%-----------------------------------------------------------------------

U = 1:10;
Y = zeros(1,1);
FI = zeros(1,2);

for i = 1:10
    u = U(i);
    fiT = [1 u];
    FI(i,:) = fiT;
    y = b0 + b1*u + b2*u^2 + (rand/10);
    Y(i) = y;
end
Y = Y';

TETA1 = inv(FI'*FI)*FI'*Y;
Y1 = FI*TETA1;

%-----------------------------------------------------------------------

U = 1:10;
Y = zeros(1,1);
FI = zeros(1,3);

for i = 1:10
    u = U(i);
    fiT = [1 u u^2];
    FI(i,:) = fiT;
    y = b0 + b1*u + b2*u^2 + (rand/10);
    Y(i) = y;
end
Y = Y';

TETA2 = inv(FI'*FI)*FI'*Y;
Y2 = FI*TETA2;

%-----------------------------------------------------------------------

U = 1:10;
Y = zeros(1,1);
FI = zeros(1,4);

for i = 1:10
    u = U(i);
    fiT = [1 u u^2 u^3];
    FI(i,:) = fiT;
    y = b0 + b1*u + b2*u^2 + (rand/10);
    Y(i) = y;
end
Y = Y';

TETA3 = inv(FI'*FI)*FI'*Y;
Y3 = FI*TETA3;

thickness = 1.5;
subplot(2,2,1), hold on;
plot(U,Y,'o');
plot(U,Y0,'k');
sizeAxis = axis;
sizeAxis(1) = 1;
axis(sizeAxis);
set(findall(gca, 'Type', 'Line'),'LineWidth',thickness);
title('(a)');
xlabel('u');
ylabel('y');
grid on;

subplot(2,2,2), hold on;
plot(U,Y,'o');
plot(U,Y1,'k');
sizeAxis = axis;
sizeAxis(1) = 1;
axis(sizeAxis);
set(findall(gca, 'Type', 'Line'),'LineWidth',thickness);
title('(b)');
xlabel('u');
ylabel('y');
grid on;

subplot(2,2,3), hold on;
plot(U,Y,'o');
plot(U,Y2,'k');
sizeAxis = axis;
sizeAxis(1) = 1;
axis(sizeAxis);
set(findall(gca, 'Type', 'Line'),'LineWidth',thickness);
title('(c)');
xlabel('u');
ylabel('y');
grid on;

subplot(2,2,4), hold on;
plot(U,Y,'o');
plot(U,Y3,'k');
sizeAxis = axis;
sizeAxis(1) = 1;
axis(sizeAxis);
set(findall(gca, 'Type', 'Line'),'LineWidth',thickness);
title('(d)');
xlabel('u');
ylabel('y');
grid on;

print('example','-dpng');