package Aquisition;

import Control.PID;
import Sensors.RGB;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.nxt.SensorPort;

public class PIDTest {

	public static final int REF = 20;
	public static final double KP = 0.5;
	public static final double KI = 0;
	public static final double KD = 0;
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";

	public static void main(String[] args) {
		NXTMotor motorCte = new NXTMotor(MotorPort.A); // Objeto para o motor de vel cte
		NXTMotor motorControl = new NXTMotor(MotorPort.B); // Objeto para o motor de vel vari�vel
		RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double u; // A��o de controle

		// Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		// pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		// motores) seja analisada separadamente.
		motorCte.setPower(50);
		while (Button.readButtons() == 0) {
			rbgValue = rgb.getSensor();
			u = pid.compute(rbgValue);
			motorControl.setPower((int) Math.round(u));
		}
		motorCte.stop();
		motorControl.stop();
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
