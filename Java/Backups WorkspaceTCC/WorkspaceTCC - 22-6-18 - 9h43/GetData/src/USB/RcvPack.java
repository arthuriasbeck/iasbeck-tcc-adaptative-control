package USB;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

public class RcvPack {

	public static final char TIME_MSG = 'A';
	public static final char RGB_MSG = 'B';
	public static final char MOTOR_B_MSG = 'C';

	public static void main(String args[]) {

		Locale locale = new Locale("pt", "BR");
		GregorianCalendar calendar = new GregorianCalendar();
		SimpleDateFormat formatador = new SimpleDateFormat("dd' de 'MMMMM' de 'yyyy' - 'HH'h'mm' '", locale);
		String currentTime = formatador.format(calendar.getTime()).trim();
		System.out.println(currentTime);

		File f = null;
		FileWriter w = null;
		BufferedWriter bw = null;
		String fileName = "results/" + currentTime + ".txt";

		try {
			f = new File(fileName);
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
		} catch (IOException e) {
			System.out.println("Erro na cria��o do arquivo");
		}

		String readValue = "";
		String pidOutput = "";
		String controlPowerA = "";
		String controlPowerB = "";
		String rgbValue = "";
		String time = "";
		String toFile = "";
		int motorBIndex = -1;
		int timeIndex = -1;
		int rbgIndex = -1;

		USBCommPC usbComm = new USBCommPC("rcvUSB.txt");
		usbComm.connect();

		while (true) {
			readValue = usbComm.receiveString();
			System.out.println("\nreadValue = " + readValue);
			if (readValue.compareTo("*") == 0) {
				break;
			}

			// A estrutura da mensagem � uma das seguintes:
			// 		1) pidOutput + rgbValue + time;
			// 		2) controlPowerA + controlPowerB + rgbValue + time;
			// Os elementos da mensagem s�o separados pelos identificadores MOTOR_B_MSG,
			// RGB_MSG e TIME_MSG, que nada mais s�o do que caracteres que delimitam a
			// localiza��o das mensagens.
			motorBIndex = readValue.indexOf(MOTOR_B_MSG);
			rbgIndex = readValue.indexOf(RGB_MSG);
			timeIndex = readValue.indexOf(TIME_MSG);

			rgbValue = readValue.substring(rbgIndex + 1, timeIndex);
			time = readValue.substring(timeIndex + 1);
			System.out.println("rgbValue = " + rgbValue);
			System.out.println("time = " + time);

			// Caso motorBIndex seja igual a -1, isso quer dizer que a mensagem n�o cont�m o
			// caractere MOTOR_B_MSG, ou seja, o controle est� sendo realizado nas duas
			// rodas, e ao inv�s da pot�ncia de cada motor, deve ser salva a a��o de
			// controle obtida na computa��o do PID.
			if (motorBIndex == -1) {
				pidOutput = readValue.substring(0, rbgIndex);
				System.out.println("pidOutput = " + pidOutput);
				toFile = pidOutput + "\t" + rgbValue + "\t" + time;
			} else if (motorBIndex != -1) {
				controlPowerA = readValue.substring(0, motorBIndex);
				controlPowerB = readValue.substring(motorBIndex + 1, rbgIndex);
				System.out.println("powerA = " + controlPowerA);
				System.out.println("powerB = " + controlPowerB);
				toFile = controlPowerA + "\t" + controlPowerB + "\t" + rgbValue + "\t" + time;
			}

			try {
				bw.write(toFile);
				bw.newLine();
				bw.flush();
			} catch (IOException e) {
				System.out.println("Erro na escrita do arquivo");
			}
		}
		System.out.println("Fim da execu��o");
		try {
			bw.close();
		} catch (IOException e) {
			System.out.println("Erro no fechamento do arquivo");
		}
		usbComm.close();
	}
}
