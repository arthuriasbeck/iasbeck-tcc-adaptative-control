Este repositório contém os códigos desenvolvidos ao longo do Projeto de Fim de Curso de Arthur Iasbeck, onde foi abordada a  implementação de um controlador adaptativo, mais especificamente de um STR (Self Tuning Regulator). O trabalho completo pode ser acessado em "Controle Adaptativo Empregado no Rastreamento de Trajetória de um Robô Autônomo.pdf". Neste documento você encontrará detalhes sobre como o projeto foi desenvolvido e sobre os resultados obtidos a partir dos códigos deste repositório. 

Foi por meio dos códigos deste repositório que uma série de figuras presentes no trabalho foi gerada. Um tabela presente em "Relação de figuras" mostra qual código foi empregue na geração de qual figura. Caso este trabalho seja útil pra você de alguma forma, o referencie utilizando 

**IASBECK, A. H., Realização do Controle de Trajetória de um Robô Autônomo Através
da Implementação de um Controlador Adaptativo. 2019. Projeto de Fim de Curso,
Universidade Federal de Uberlândia, Uberlândia.**


Qualquer dúvida entre em contato pelo e-mail
arthuriasbeck@gmail.com
