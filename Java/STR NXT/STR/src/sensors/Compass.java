package sensors;

import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.addon.CompassHTSensor;
import lejos.util.Delay;

/**
 * O que dizer da b�ssola ? Bem, ela � boa mas tem que ser usada com cautela.
 * ela sofre interfer�ncia muito facilmente. � poss�vel us�-la se voc� utilizar
 * a calibragem adequada. B�ssolas nunca calibradas apresentaram um desempenho
 * muito bom nesta competi��o. Mas tome sempre muito cuidado. B�ssola � um
 * sensor muito bom na programa��o. F�cil de ser utilizado. Mas no que se refere
 * ao hardware, d� muitas dores de cabe�a. Resumindo, n�o � pecado usar a
 * b�ssola. Mas n�o dependa s� dela. Tenha sempre outras cartas na manga.
 * Acredite em n�s, pois aprendemos da pior maneira poss�vel (pergunte a um
 * membro antigo sobre a competi��o LARC de 2015 e espere enquanto ele explica,
 * em meio a l�grimas, o que estou querendo dizer).
 * 
 * @author SEK - EDROM
 *
 */

public class Compass implements Sensor {

	private CompassHTSensor compass;
	private int discontinuityCount = 0;
	private double lastCompassValue;
	private long timeLastAcquisition = 0;

	public Compass(SensorPort port) {
		compass = new CompassHTSensor(port);
	}

	/*
	 * Esta fun��o retorna o valor da b�ssola. Aqui a continuidade � corrigida
	 * utilizando um contador de descontinuidade (ideia do Jo�o). Toda vez que este
	 * contador passa do 359 pro 0, ele � acrescido em uma unidade e caso ele passe
	 * do 0 para o 359, ele � decrescido em uma unidade. Assim sendo, o valor da
	 * descontinuidade corrigido ser�:
	 * 
	 * compassValue = currentCompassValue + discontinuityCount*360
	 * 
	 * Se voc� estiver indo no sentido hor�rio, voc� chega no 359 e at� esse momento
	 * o contador � zero. Neste caso (compassValue = currentCompassValue). Quando
	 * voc� passa do 359 para o 0, voc� deveria ler 0, mas isso n�o acontece porque
	 * neste caso o contador (currentCompassValue) � acrescido de uma unidade e o
	 * retorno da fun��o passa a ser (currentCompassValue + 360), ou seja, ao inves
	 * da fun�a� retornar 0, ela vai retornar 360, depois 361 e assim por diante. O
	 * mesmo vai acontecer se voc� estiver girando no sentido anti-hor�rio.
	 * Corrigimos a descontinuidade.
	 */
	public double getSensor() {
		double currentCompassValue;
		if(System.currentTimeMillis() - timeLastAcquisition > 2000) {
			discontinuityCount = 0;
		}
		timeLastAcquisition = System.currentTimeMillis();
		currentCompassValue = compass.getDegrees();
		if(lastCompassValue - currentCompassValue < -200) discontinuityCount--;
		if(lastCompassValue - currentCompassValue > 200) discontinuityCount++;
		lastCompassValue = currentCompassValue;
		return currentCompassValue + discontinuityCount*360;
	}

	/**
	 * Este m�todo deve ser utilizado para corrigir a descontinuidade da b�ssola
	 */
	public static double correctedAngle(double angle) {
		while (angle > 359)
			angle -= 360;
		while (angle <= 0)
			angle += 360;
		return (angle);
	}

	/**
	 * Este m�todo deve ser utilizado para corrigir a descontinuidade que se reflete no erro
	 */
	public static double correctedError(double error) {
		while (error > 180)
			error -= 360;
		while (error <= -180)
			error += 360;
		return error;
	}

	/**
	 * M�todo para calibra��o da b�ssola. Requer alguns ajustes. Basicamente a
	 * calibra��o da b�ssola consiste em realizar um giro completo, saindo do
	 * norte e chegando at� o norte novamente. Quando o sensor detecta o norte
	 * duas vezes, ele determina quanta mudan�a no campo magn�tico ele poder�
	 * considerar 1 grau.
	 */
	public void compassCalibrate() {
		compass.startCalibration();
		Motor.A.setSpeed(200);
		Motor.C.setSpeed(200);
		Motor.C.forward();
		Motor.A.backward();
		Delay.msDelay(30000);
		compass.stopCalibration();
	}

	public static void main(String args[]) {
		Compass compass = new Compass(SensorPort.S2);
		while(true) {
			if(Button.ENTER.isDown()) {
				while(Button.ENTER.isDown());
				LCD.clear();
				LCD.drawString("Aguardando", 0, 0);
				Button.waitForAnyPress();
				while(Button.readButtons() != 0);
			}
			LCD.clear();
			LCD.drawString(""+compass.getSensor(), 0, 0);
			Delay.msDelay(200);
		}
	}
}

