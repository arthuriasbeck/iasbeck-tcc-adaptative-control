function plotResults(numLoops, T, uArray, yArray, yeArray, errEstArray, a1Array, a2Array, b0Array, b1Array)
    % Mostrando resultados da simula��o da planta
    time = (0:numLoops-1)*T;
    lX = 'Tempo(s)';
    lY = 'Entrada';
    title = 'Entrada do processo';
    completeSubPlot(1,2,1,1,time,uArray,'',1.5,lX,lY,title,'grid',0,1,'','')

    lY = 'Sa�da';
    title = 'Sa�da do processo';
    file = 'Simula��o do seguidor com entrada real';
    completeSubPlot(1,2,1,2,time,yArray,'',1.5,lX,lY,title,'grid',0,1,file,'')

    % Mostrando dados da estima��o
    title = 'Sa�da do processo x Estima��o';
    file = 'Resultados da estima��o';
    completePlot(2,time,yArray,'',1.5,lX,lY,title,'grid',0,1,'','')
    completePlot(2,time,yeArray,'ko',1.5,lX,lY,title,'grid',0,1,'','')
    legend('Sa�da real','Sa�da estimada');
    completePlot(2,time,yeArray,'ko',1.5,lX,lY,title,'grid',0,1,file,'')
    
    % Mostrando evolu��o dos par�metros ao longo do tempo
    % Par�metro a1
    lY = 'a1';
    a1Real = ones(1,numLoops)*(-1.0002);
    a1Last = a1Array(end);
    title = strcat({'a1 = '}, num2str(a1Last),{' ('}, '-1,0002',{ ')'});
    completeSubPlot(3,2,1,1,time,a1Array,'',1.5,lX,lY,title,'',0,1,'','');
    completeSubPlot(3,2,1,1,time,a1Real,'--k',1.5,lX,lY,title,'',0,1,'','');

    % Par�metro a2
    lY = 'a2';
    a2Real = ones(1,numLoops)*(-0.0008);
    a2Last = a2Array(end);
    title = strcat({'a2 = '}, num2str(a2Last),{' ('}, num2str(-0.0008),{ ')'});
    file = 'Evolu��o dos par�metros a';
    completeSubPlot(3,2,1,2,time,a2Array,'',1.5,lX,lY,title,'',0,1,'','');
    completeSubPlot(3,2,1,2,time,a2Real,'--k',1.5,lX,lY,title,'',0,1,file,'');

    % Par�metro b0
    lY = 'b0';
    b0Real = ones(1,numLoops)*(-0.0244);
    b0Last = b0Array(end);
    title = strcat({'b0 = '}, num2str(b0Last),{' ('}, num2str(-0.0244),{ ')'});
    completeSubPlot(4,2,1,1,time,b0Array,'',1.5,lX,lY,title,'',0,1,'','');
    completeSubPlot(4,2,1,1,time,b0Real,'--k',1.5,lX,lY,title,'',0,1,'','');

    % Par�metro b1
    lY = 'b1';
    b1Real = ones(1,numLoops)*(0.1503);
    b1Last = b1Array(end);
    title = strcat({'b1 = '}, num2str(b1Last),{' ('}, num2str(0.1503),{ ')'});
    file = 'Evolu��o dos par�metros b';
    completeSubPlot(4,2,1,2,time,b1Array,'',1.5,lX,lY,title,'',0,1,'','');
    completeSubPlot(4,2,1,2,time,b1Real,'--k',1.5,lX,lY,title,'',0,1,file,'');
    
    % Mostrando a evolu��o do erro de estima��o
    lY = 'Erro';
    title = 'Erro de estima��o';
    file = 'Erro de estima��o';
    e = zeros(1,numLoops);
    completePlot(5,time,e,'--k',1.5,lX,lY,title,'grid',0,1,'','')
    completePlot(5,time,errEstArray,'',1.5,lX,lY,title,'grid',0,1,file,'')
end