package classTest;

import control.PolePlaceControl;

public class PolePlaceControlTest {
	public static void main(String args[]) {
		double T = 0.05;
		double tSim = 3;

		// Par�metros da planta
		double a1 = -1.0002;
		double a2 = -0.0008;
		double b0 = -0.0244;
		double b1 = 0.1503;

		// Par�metros do modelo
		double am1 = -1.3205;
		double am2 = 0.4966;

		// Vari�veis para execu��o
		// Sa�das para a planta
		double y = 0;
		double y_1 = 0;
		double y_2 = 0;

		// Entradas para a planta
		double u = 0;
		double u_1 = 0;
		double u_2 = 0;

		// Sa�das para o modelo
		double ym = 0;
		double ym_1 = 0;
		double ym_2 = 0;

		// Entradas para o modelo
		double uc_1 = 0;
		double uc_2 = 0;
		double uc = 1;
		int loops = (int) Math.round(tSim / T);

		double beta;

		PolePlaceControl controller = new PolePlaceControl();

		for (int i = 0; i < loops; i++) {

			// Computando a��o de controle e sa�das da planta e do modelo
			controller.computeControlPar(a1, a2, b0, b1, am1, am2);
			y = -a1 * y_1 - a2 * y_2 + b0 * u_1 + b1 * u_2;
			beta = controller.getBeta();
			ym = -am1 * ym_1 - am2 * ym_2 + beta * b0 * uc_1 + beta * b1 * uc_2;
			u = controller.computeControlAct(y, uc);

			// Atualilzando vari�veis que armazenam entradas e sa�das anteriores
			uc_2 = uc_1;
			uc_1 = uc;
			ym_2 = ym_1;
			ym_1 = ym;
			u_2 = u_1;
			u_1 = u;
			y_2 = y_1;
			y_1 = y;
			
			// Mostrando informa��es da execu��o
			System.out.println(y + "\t" + ym);
		}
	}
}
