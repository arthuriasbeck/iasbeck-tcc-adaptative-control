package USB;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class GetDataMain {

	public static final char RGB_MSG = 'A';
	public static final char TIME_MSG = 'B';

	public static void main(String args[]) {
		USBComm usb = new USBComm();
		File f;
		FileWriter w;
		BufferedWriter bw = null;
		String fileName = "teste.txt";
		try {
			f = new File(fileName);
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
		} catch (IOException e) {
			System.out.println("Erro inicializando arquivo");
		}

		usb.connect();
		String msg;
		String y = null;
		String u = null;
		String time = null;
		long nRcv = 0;
		ArrayList<String> yData = new ArrayList<String>();
		ArrayList<String> uData = new ArrayList<String>();
		ArrayList<String> timeData = new ArrayList<String>();
		int indexTime;
		int indexRGB;

		System.out.println("Iniciando recebimento");
		while (true) {
			System.out.println(nRcv);
			msg = usb.receiveString();
			System.out.println(msg);
			System.out.println(".");
			if (msg.charAt(0) == '*')
				break;
			indexRGB = msg.indexOf(RGB_MSG);
			indexTime = msg.indexOf(TIME_MSG);
			u = msg.substring(0, indexRGB);
			y = msg.substring(indexRGB + 1, indexTime);
			time = msg.substring(indexTime + 1);
			System.out.println("u = " + u + "\t" + "y = " + y + "\t" + "t = " + time);
			yData.add(y);
			uData.add(u);
			timeData.add(time);
			nRcv++;
		}
		
		try {
			for (int i = 0; i < uData.size(); i++) {
				bw.write(uData.get(i) + "\t" + yData.get(i) + "\t" + timeData.get(i));
				bw.newLine();
				bw.flush();
			}

		} catch (IOException e) {
			System.out.println("Erro escrevendo no arquivo");
		}
		try {
			bw.close();
		} catch (IOException e) {
			System.out.print("Erro no fechamento do arquivo");
		}
		usb.close();
		System.out.println("Fim da execu��o");
		System.out.println(nRcv + " pares entrada/sa�da recebidos");
	}
}
