package control;

import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import motors.NewMotor;
import sensors.Compass;

public class STRExamples {
	
	public static void controlExample() {
		Compass compass = new Compass(SensorPort.S2);
		NewMotor motorA = new NewMotor(MotorPort.A, true);
		NewMotor motorB = new NewMotor(MotorPort.B, true);
		STRControl strControl = new STRControl(compass, motorA, motorB);
		
		double y, u, uc;
		uc = 20; // Refer�ncia para a b�ssola
		while(true) {
			y = compass.getSensor();
			u = strControl.control(y, uc);
			motorA.setPower((int) Math.round(40 + u));
			motorB.setPower((int) Math.round(40 - u));
			motorA.forward();
			motorB.forward();
		}
	}
	
	public static void calibrateExample() {
		Compass compass = new Compass(SensorPort.S2);
		NewMotor motorA = new NewMotor(MotorPort.A, true);
		NewMotor motorB = new NewMotor(MotorPort.B, true);
		STRControl strControl = new STRControl(compass, motorA, motorB);
		
		double Mp = 0.1;
		double ta = 1;
		double T = 0.05;
		long refTime = 4000;
		long simTime = refTime * 4;
		strControl.calibrate(refTime, simTime, Mp, ta, T, false);
	}
	
	public static void controlTestExample() {
		Compass compass = new Compass(SensorPort.S2);
		NewMotor motorA = new NewMotor(MotorPort.A, true);
		NewMotor motorB = new NewMotor(MotorPort.B, true);
		STRControl strControl = new STRControl(compass, motorA, motorB);
		
		long refTime = 4000;
		long simTime = refTime * 4;
		strControl.controlTest(refTime, simTime);
	}
	
	public static void main(String args[]) {
		controlExample();
	}
}
