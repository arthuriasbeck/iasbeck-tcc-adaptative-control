package Aquisition;

import Control.PID;
import Motors.NewMotor;
import Sensors.RGB;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.util.Delay;

public class PIDTest {

	public static final int REF = 420;
	public static final double KP = 0.14;
	public static final double KI = 0.005;
	public static final double KD = 0;
	public static final int MIN_POWER = 40;
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";

	public static void main(String[] args) {
		NewMotor motorA = new NewMotor(MotorPort.A, true); 
		NewMotor motorB = new NewMotor(MotorPort.B, true); 
		RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double u; // A��o de controle
		long loopTime = 0; // Tempo do in�cio do loop atual
		int controlPower = 0; // A��o de controle sobre o motor

		LCD.clear();
		LCD.drawString("Pressione para \niniciar a \nsimulacao...", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0) {

		}

		LCD.clear();
		LCD.drawString("Simulacao \niniciada !", 0, 0);

		/*
		 * Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		 * pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		 * motores) seja analisada separadamente.
		 */
		motorB.setPower(MIN_POWER);
		motorB.forward();

		/*
		 * A ideia aqui � que a cada n segundos, sendo n um n�mero aleat�rio que vai de
		 * 1 a 5, uma perturba��o � adicionada na entrada da planta, ou seja, na
		 * pot�ncia do motor controlado. A perturba��o � um valor alet�rio que vai de 10
		 * a 40.
		 */
		while (Button.readButtons() == 0) {
			loopTime = System.currentTimeMillis();
			rbgValue = rgb.getSensor();
			u = pid.compute(rbgValue);
			controlPower = MIN_POWER - (int) Math.round(u);
			motorA.setPower(controlPower);
			LCD.clear();
			LCD.drawString("" + u, 0, 0);
			motorA.forward();
			Delay.msDelay(50 - (System.currentTimeMillis() - loopTime));
		}
		motorA.stop();
		motorB.stop();
		Delay.msDelay(300);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
