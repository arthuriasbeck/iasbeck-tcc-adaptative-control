package Motors;

import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;

public class NewMotor {

	private boolean inverted = false;
	private NXTMotor motor;
	
	public NewMotor(MotorPort port) {
		this(port, false);
	}
	
	public NewMotor(MotorPort port, boolean inverted) {
		this.inverted = inverted;
		this.motor = new NXTMotor(port);
	}
	
	public void setPower(int power) {
		motor.setPower(power);
	}
	
	public void forward() {
		if(inverted) {
			motor.backward();
		}
		else if(!inverted){
			motor.forward();
		}
	}
	
	public void backward() {
		if(inverted) {
			motor.forward();
		}
		else if(!inverted){
			motor.backward();
		}
	}
	
	public void stop() {
		motor.stop();
	}
	
}
