package Aquisition;

import Control.PID;
import Motors.NewMotor;
import Sensors.RGB;
import USB.USBComm;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.util.Delay;

public class AquisitionUSB {

	public static final int LEFT = 1;
	public static final int RIGHT = -1;
	
	public static final boolean USB_COMM = true; // Set para false para desativar a comunica��o USB
	public static final boolean NOISE = false; // Set para false para remover os ru�dos.
	public static final boolean MOTORS = true; // Set para false para desativar os motores
	public static final char CTE_MOTOR = 'A'; // Set para A para que o motor A fique constante
	public static final int LINE_SIDE = RIGHT; // Set para RIGHT para seguir pelo lado direito
	
	public static final int REF = 500;
	public static final double KP = 0.11;
	public static final double KI = 0.001;
	public static final double KD = 0;
	public static final int MIN_POWER = 40;
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";
	public static final String MOTOR_B_MSG = "C";
	public static final int NOISE_TIME = 50;

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		NewMotor motorA = new NewMotor(MotorPort.A, true); // Representa o motor A
		NewMotor motorB = new NewMotor(MotorPort.B, true); // Representa o motor B
		RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		USBComm usb = new USBComm(); // Objeto para comunica��o USB
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double pidOutput; // Sa�da do controlador PID
		boolean firstLoop = true; // Informa se estamos no primeiro loop
		long time = 0; // Tempo de in�cio da simula��o
		long pastTime = 0; // Tempo total de simula��o
		String usbMsg; // Mensagem enviada ao PC atrav�s do USB
		int controlPowerA = 0; // A��o de controle sobre o motor A
		int controlPowerB = 0; // A��o de controle sobre o motor B
		long timeLastNoise = System.currentTimeMillis(); // Tempo em que ocorreu a �ltima perturba��o
		long timeToNoise = 5000; // Tempo at� a pr�xima perturba��o
		long timeStartNoise = 0; // Tempo em que a perturba��o come�ou a agir
		boolean noising = false; // Indica se a perturba��o est� atuando ou n�o
		int noise = 0; // Amplitude da perturba��o
		long stopTime; // Tempo que o seguidor fica parado

		/*
		 * Esta fun��o � iniciada e aguarda at� que o computador estabele�a com o CLP a
		 * comunica��o USB.
		 */
		if (USB_COMM) {
			usb.connect();
		}

		LCD.clear();
		LCD.drawString("Pressione para \niniciar a \nsimulacao...", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0);

		LCD.clear();
		LCD.drawString("Simulacao \niniciada !", 0, 0);

		/*
		 * Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		 * pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		 * motores) seja analisada separadamente.
		 */

		time = System.currentTimeMillis();
		while (!Button.ESCAPE.isDown()) {
			if (Button.ENTER.isDown()) {
				stopTime = System.currentTimeMillis();
				while (Button.ENTER.isDown());
				motorA.stop();
				motorB.stop();
				LCD.clear();
				LCD.drawString("Pressione para \ncontinuar...", 0, 0);
				while (!Button.ENTER.isDown() && !Button.ESCAPE.isDown());
				if (Button.ESCAPE.isDown()) {
					while (Button.ESCAPE.isDown());
					break;
				} else if (Button.ENTER.isDown()) {
					while (Button.ENTER.isDown());
					time += System.currentTimeMillis() - stopTime;
				}
				LCD.clear();
			}
			/*
			 * Controle do seguidor
			 */
			rbgValue = rgb.getSensor();
			pidOutput = pid.compute(rbgValue);
			/*
			 * Aqui o disturbio � inserido a cada timeToNoise segundos e a cada vez que isso
			 * acontece, o tempo para a inser��o do pr�ximo ru�do � recalculado. A ideia
			 * aqui � que a cada n segundos, sendo n um n�mero aleat�rio que vai de 1 a 5,
			 * uma perturba��o � adicionada na entrada da planta, ou seja, na pot�ncia do
			 * motor controlado. A perturba��o � um valor alet�rio que vai de 10 a 40.
			 */

			if (NOISE && (System.currentTimeMillis() - timeLastNoise > timeToNoise || noising)) {
				if (!noising) {
					noising = true;
					Sound.playTone(1000, NOISE_TIME);
					timeStartNoise = System.currentTimeMillis(); // Tempo de in�cio do ru�do
					timeToNoise = Math.round(Math.random() * 3000 + 3000); // Tempo para o pr�ximo ru�do
					noise = (int) Math.round(Math.random() * 30 + 10); // Amplitude do ru�do
					LCD.clear();
					LCD.drawString("time=" + timeToNoise, 0, 0);
					LCD.drawString("noise = " + noise, 0, 1);
				}
				/*
				 * O ru�do cessa depois que se passam NOISE_TIME ms desde que ele come�ou a
				 * atuar (timeStartNoise).
				 */
				if (System.currentTimeMillis() - timeStartNoise > NOISE_TIME) {
					noising = false;
					timeLastNoise = System.currentTimeMillis();
				}
				if (CTE_MOTOR == 'B') {
					controlPowerA = 0;
					controlPowerB = MIN_POWER;
				} else if (CTE_MOTOR == 'A') {
					controlPowerA = MIN_POWER;
					controlPowerB = 0;
				}
			} else {
				if (CTE_MOTOR == 'B') {
					controlPowerA = MIN_POWER - LINE_SIDE * (int) Math.round(pidOutput);
					controlPowerB = MIN_POWER;
				} else if (CTE_MOTOR == 'A') {
					controlPowerA = MIN_POWER;
					controlPowerB = MIN_POWER + LINE_SIDE * (int) Math.round(pidOutput);
				}
			}
			if (controlPowerA > 100) {
				controlPowerA = 100;
			}
			if (controlPowerA < -100) {
				controlPowerA = -100;
			}
			
			/*
			 * Caso a vari�vel MOTORS seja igual a false, os motores A e B n�o devem se
			 * movimentar durante a execu��o do c�digo.
			 */
			if (!MOTORS) {
				controlPowerA = 0;
				controlPowerB = 0;
			}

			motorA.setPower(controlPowerA);
			motorA.forward();
			motorB.setPower(controlPowerB);
			motorB.forward();
			LCD.clear(1);
			LCD.drawString("" + controlPowerA, 0, 1);

			/*
			 * Envio da mensagem
			 */
			if (USB_COMM) {
				usbMsg = controlPowerA + MOTOR_B_MSG + controlPowerB + RGB_MSG + rbgValue + TIME_MSG;
				if (firstLoop) {
					time = System.currentTimeMillis();
					pastTime = System.currentTimeMillis() - time;
					usbMsg += pastTime;
					firstLoop = false;
				} else {
					while ((System.currentTimeMillis() - time) < (pastTime + 50)) {

					}
					pastTime = System.currentTimeMillis() - time;
					usbMsg += pastTime;
				}
				usb.send(usbMsg);
				LCD.clear(2);
				LCD.clear(3);
				LCD.drawString("time=" + time, 0, 2);
				LCD.drawString("pastT=" + pastTime, 0, 3);
			}
		}
		motorA.stop();
		motorB.stop();
		if (USB_COMM) {
			usbMsg = "*";
			usb.send(usbMsg);
		}
		Delay.msDelay(50);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
