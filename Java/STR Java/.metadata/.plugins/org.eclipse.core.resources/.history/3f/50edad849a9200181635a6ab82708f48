package classTest;

import control.STR;
import utilities.Matrix;

/**
 * Esta classe � uma tradu��o direta dos c�digos em Matlab que implementam o
 * controlador STR. Note que aqui n�o foi utilizada nenhuma classe previamente
 * desenvolvida (como LeatSquares ou STR) mas os c�digos destas classes foram
 * diretamente inseridos aqui.
 * 
 * @author Arthur Iasbeck
 *
 */

public class STRTest {
	public static void main(String args[]) {
		// Este programa tem como finalidade implementar o controlador desenvolvido
		// atrav�s do m�todo pole placement, que depende dos par�metros da planta.
		// Aqui, valores de sa�da que na implementa��o final ser�o obtidos do rob�,
		// ser�o obtidos atrav�s de uma equa��o diferen�a (que foi gerada atrav�s de
		// uma estima��o experimental). Com os valores de sa�da, o m�todo dos
		// m�nimos quadrados tentar� estimar os par�metros da planta e atrav�s da
		// utiliza��o destes par�metros, ser� obtida a a��o de controle u.

		// Par�metros para execu��o
		double T = 0.05; // Tempo de amostragem
		double tSim = 10; // Tempo de simula��o
		double tInput = 3; // Tempo para que ocorra uma mudan�a na entrada do sistema
		double ucAmp = 90; // Determina a amplitude da entrada degrau
		double ucSig = 1; // Determina se o degrau ser� positivo ou negativo

		// Par�metros do modelo
		double am1 = -1.3205;
		double am2 = 0.4966;
		double a0 = 0;

		// Inicializa��o de vari�veis utilizadas na simula��o da planta
		double y = 0; // y(t)
		double y_1 = 0; // y(t-1)
		double y_2 = 0; // y(t-2)
		double u = 0; // u(t)
		double u_1 = 0; // u(t-1)
		double u_2 = 0; // u(t-2)

		// Inicializa��o das vari�veis utilizadas na simula��o do modelo
		double ym = 0; // ym(t)
		double ym_1 = 0; // ym(t-1)
		double ym_2 = 0; // ym(t-2)
		double uc = 0; // uc(t)
		double uc_1 = 0; // uc(t-1)
		double uc_2 = 0; // uc(t-2)

		// Inicializa��o das vari�veis utilizadas na estima��o
		double ye = 0, errEst;
		Matrix fi, P, teta;
		P = Matrix.random(4, 4);
		fi = new Matrix(4, 1);
		teta = Matrix.random(4, 1);

		double[][] data = new double[4][1];
		double pScalarAux; // pScalarAux = fi'*P*fi
		double pScalar; // pScalar = (1/(1 + pScalarAux));
		Matrix pTerm1; // Vari�vel utilizada no c�lculo do escalar pScalar1
		Matrix pTerm2; // Vari�vel utilizada no c�lculo de P
		Matrix fiT;
		Matrix K;
		Matrix yeMatrix;
		Matrix tetaTerm;

		// Inicializa��o dos par�metros da planta
		double a1 = 0;
		double a2 = 0;
		double b0 = 0;
		double b1 = 0;

		double s0;
		double s1;
		double r1;
		double beta;
		
		STR strControl = new STR();

		// Vari�veis para controle de execu��o
		int numLoops = (int) Math.round(tSim / T);

		for (int loops = 0; loops < numLoops; loops++) {
			// Computando sa�da da planta
			y = 1.0002 * y_1 + 0.0008 * y_2 - 0.0244 * u_1 + 0.1503 * u_2;

			// Atualiza��o dos par�metros do modelo
//			data[0][0] = -y_1;
//			data[1][0] = -y_2;
//			data[2][0] = u_1;
//			data[3][0] = u_2;
//			fi.setMatrix(data); // Setando fi
//			fiT = fi.transpose(); // Obtendo fi'
//
//			// pScalarAux = fi'*P*fi
//			pTerm1 = fiT.times(P);
//			pTerm1 = pTerm1.times(fi);
//			pScalarAux = pTerm1.getElement(0, 0); // Obtendo o escalar restante das opera��es acima
//
//			// pScalar = (1/(1 + pScalarAux));
//			pScalar = 1 / (1 + pScalarAux);
//
//			// P*fi*pScalar*fi'*P
//			pTerm2 = P.times(fi);
//			pTerm2 = pTerm2.times(pScalar);
//			pTerm2 = pTerm2.times(fiT);
//			pTerm2 = pTerm2.times(P);
//			P = P.minus(pTerm2);
//
//			// K = P*fi;
//			K = P.times(fi);
//
//			// ye = fi'*teta;
//			yeMatrix = fiT.times(teta);
//			ye = yeMatrix.getElement(0, 0);
//			errEst = y - ye;
//
//			// tetaTerm = K*errEst;
//			tetaTerm = K.times(errEst);
//
//			// teta = teta + tetaTerm;
//			teta = teta.plus(tetaTerm);
//
//			// Atualizando par�metros da planta
//			a1 = teta.getElement(0, 0);
//			a2 = teta.getElement(1, 0);
//			b0 = teta.getElement(2, 0);
//			b1 = teta.getElement(3, 0);
//
//			// Atualizando par�metros do controlador
//			s0 = (b1 * (a0 * am1 - a2 - am1 * a1 + Math.pow(a1, 2) + am2 - a1 * a0))
//					/ (Math.pow(b1, 2) - a1 * b0 * b1 + a2 * Math.pow(b0, 2))
//					+ (b0 * (am1 * a2 - a1 * a2 - a0 * am2 + a0 * a2))
//							/ (Math.pow(b1, 2) - a1 * b0 * b1 + a2 * Math.pow(b0, 2));
//			s1 = (b1 * (a1 * a2 - am1 * a2 + a0 * am2 - a0 * a2))
//					/ (Math.pow(b1, 2) - a1 * b0 * b1 + a2 * Math.pow(b0, 2))
//					+ (b0 * (a2 * am2 - Math.pow(a2, 2) - a0 * am2 * a1 + a0 * a2 * am1))
//							/ (Math.pow(b1, 2) - a1 * b0 * b1 + a2 * Math.pow(b0, 2));
//			r1 = (a0 * am2 * Math.pow(b0, 2) + (a2 - am2 - a0 * am1) * b0 * b1 + (a0 + am1 - a1) * Math.pow(b1, 2))
//					/ (Math.pow(b1, 2) - a1 * b0 * b1 + a2 * Math.pow(b0, 2));
//			beta = (1 + am1 + am2) / (b0 + b1);

			// Computando as entradas
			if (loops % (tInput / T) == 0) {
				if (ucSig == 1) {
					ucSig = -1;
				} else if (ucSig == -1) {
					ucSig = 1;
				}
			}
			uc = ucAmp * ucSig;
			
			u = strControl.compute(y, uc);
			//u = -r1 * u_1 + beta * uc + beta * a0 * uc_1 - s0 * y - s1 * y_1;
			

			y_2 = y_1;
			y_1 = y;
			u_2 = u_1;
			u_1 = u;
			ym_2 = ym_1;
			ym_1 = ym;
			uc_2 = uc_1;
			uc_1 = uc;
			
			System.out.println(y + "\t" +  ym + "\t" + uc + "\t" + u);
		}
	}
}