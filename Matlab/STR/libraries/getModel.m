function [am1 ,am2] = getModel(ta, Mp, T)
    Mp = Mp/100;
    qsi = sqrt(((log(Mp))^2)/((log(Mp))^2 + pi^2));
    wn = (4)/(qsi*ta);
    t = T/2;
    A = -qsi*wn;
    B = wn*sqrt(1 - qsi^2);
    C = (1 - (t*A)^2 - (t*B)^2)/((1 - t*A)^2 + (t*B)^2);
    D = (2*t*B)/((1 - t*A)^2 + (t*B)^2);
    am1 = -2*C;
    am2 = C^2 + D^2;
end