package USB;

import lejos.util.Delay;

public class TestSendPack {
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";

	public static void main(String args[]) {
		double u;
		double rgbValue;
		long time;
		String usbMsg = "";
		USBComm usbComm = new USBComm();
		usbComm.connect();
		for (int i = 0; i < 10; i++) {
			u = Math.random() * 100;
			rgbValue = Math.random() * 1023;
			time = System.currentTimeMillis();
			usbMsg = u + RGB_MSG + rgbValue + TIME_MSG + time;
			usbComm.send(usbMsg);
			Delay.msDelay(50);
		}
		usbComm.send("*");
	}
}
