% Este c�digo mostra a implementa��o de um STR que cancela o zero da
% planta do processo controlado.

clear; clc; close all;
addpath('../libraries/PlotData');
% Par�metros da planta e do modelo
b0 = 0.1065;
b1 = 0.0902;
a1 = -1.6065;
a2 = 0.6065;

% Par�metros do modelo
bm0 = 0.1761;
am1 = -1.3205;
am2 = 0.4966;

% Par�metros de execu��o:}
T = 0.5;
tSim = 15;

% Vari�veis para execu��o
% Sa�das para a planta 
y = 0;
y_1 = 0;
y_2 = 0;
% Entradas para a planta 
u = 0;
u_1 = 0;
u_2 = 0;
% Sa�das para o modelo 
ym = 0;
ym_1 = 0;
ym_2 = 0;
% Entradas para o modelo
uc = 0;
uc_1 = 0;

uc = 1;
yArray = zeros(1,1);
ymArray = zeros(1,1);
uArray = zeros(1,1);
loops = tSim/T;

for i = 1:loops
    % Computando a��o de controle e sa�das da planta e do modelo
    y = -a1*y_1 - a2*y_2 + b0*u_1 + b1*u_2;
    ym = -am1*ym_1 - am2*ym_2 + bm0*uc_1;
    u = (bm0/b0)*uc - (b1/b0)*u_1 + ((a1 - am1)/(b0))*y + ((a2 - am2)/(b0))*y_1;
      
    % Armaenando informa��es sobre execu��o
    yArray(i) = y;
    ymArray(i) = ym;
    uArray(i) = u;
    
    % Atualilzando vari�veis que armazenam entradas e sa�das anteriores
    uc_1 = uc;
    ym_2 = ym_1;
    ym_1 = ym;
    u_2 = u_1;
    u_1 = u;
    y_2 = y_1;
    y_1 = y;
end

% Motrando graficamente os resultados 
time = (0:i-1)*T;
x = 'Tempo(s)';
y = 'Sa�da';
title = 'Respostas do modelo e do sistema controlado';
f = 'zeroCancelOutput';
completePlotDigital(1,time,yArray,'bo',1.5,x,y,title,'grid',0,1,'','',0);
completePlotDigital(1,time,ymArray,'r',1.5,x,y,title,'grid',0,1,'','',1);
lgd = legend('Sa�da real','Sa�da desejada');
lgd.Position(2) = lgd.Position(2) - 0.1;
completePlotDigital(1,time,ymArray,'r',1.5,x,y,title,'grid',0,1,f,'',1);

y = 'Entrada';
title = 'Entrada';
f = 'zeroCancelInput';
completePlotDigital(2,time,uArray,'b',1.5,x,y,title,'grid',0,1,f,'',1);