package file;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import lejos.nxt.Button;
import lejos.nxt.LCD;

/**
 * Esta classe foi criada para realizar a escrita e a leitura em arquivos. A
 * classe recebe uma ArrayList de doubles e salva todos eles num arquivo. Caso
 * qualquer valor seja salvo no arquivo depois que ele � aberto, tudo que havia
 * antes � perdido, no entando, caso a classe seja iniciada e seja realizada
 * apenas a leitura, uma ArrayList de doubles contendo todos os dados no arquivo
 * � retornada.
 * 
 * @author Iasbeck
 *
 */
public class FileHandler {

	private File file;

	public FileHandler(String fileName) {
		file = new File(fileName);
	}

	public void saveInFile(ArrayList<Double> references) {
		FileOutputStream out;
		try {
			out = new FileOutputStream(file);
			DataOutputStream dataOut = new DataOutputStream(out);
			for (int i = 0; i < references.size(); i++) {
				dataOut.writeDouble(references.get(i));
			}
			dataOut.flush();
			out.close();
		} catch (IOException e) {
			LCD.drawString("Erro na escrita", 0, 0);
		}
	}

	public ArrayList<Double> loadFromFile() {
		ArrayList<Double> references = new ArrayList<Double>();
		if (file.exists()) {
			try {
				FileInputStream is = new FileInputStream(file);
				DataInputStream dataIn = new DataInputStream(is);

				for (int i = 0; i < sizeFile(); i++) {
					references.add(dataIn.readDouble());
				}
				dataIn.close();
			} catch (IOException e) {
				LCD.drawString("Erro na leitura", 0, 0);
			}
			return (references);
		} else {
			LCD.drawString("Arquivo nao existe", 0, 0);
			return references;
		}
	}

	public void deleteFile() {
		file.delete();
	}

	public long sizeFile() {
		// Lembre-se que cada double ocupa 8 bytes no arquivo e que a fun��o length()
		// retorna quantos bytes j� foram escritos no arquivo.
		return (file.length() / 8);
	}

	public long sizeFileBytes() {
		return (file.length());
	}

	/**
	 * Fun��o utilizada no teste da classe File
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		FileHandler fileTest = new FileHandler("test.dat");
		ArrayList<Double> d = new ArrayList<Double>();
		d.add(1.0);
		d.add(2.0);
		d.add(3.0);
		d.add(4.0);
		d.add(5.0);
		d.add(6.0);
		d.add(7.0);
		fileTest.saveInFile(d);

		LCD.clear();
		LCD.drawString("Size=" + fileTest.sizeFile() + "(" + fileTest.sizeFileBytes() + " bytes)", 0, 0);

		ArrayList<Double> e = new ArrayList<Double>();
		e = fileTest.loadFromFile();
		for (int i = 0; i < e.size(); i++) {
			LCD.drawString("" + e.get(i), 0, i + 1);
		}
		Button.waitForAnyPress();
	}
}
