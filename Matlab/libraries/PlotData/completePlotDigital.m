function completePlot(figureNumber,x,y,plotConfig,thickness,lableX,lableY,plotTitle,gridCte,xMin,xMax,saveName,fullScreen,digital)
    % figureNumber - N�mero da figura em que se encontra o plot
    % x - Valores de x
    % y - Valores de y
    % plotConfig - Configura��es adicionais do plot. Se n�o houverem
    %              configura��es adicionais para o plot (cor da linha, 
    %              tipo de linha...) o usu�rio deve inserir '' neste 
    %              par�metro
    % thickness - Espessura da linha do plot
    % lableX - Titulo do eixo X
    % lableY - Titulo do eixo Y
    % plotTitle - T�tulo do plot
    % gridCte - Igual a 'grid' se o usu�rio desejar um grid no plot
    % xMax - Valor m�ximo de X, em porcentagem, a ser mostrado no plot
    % xMin - Valor m�nimo de X, em porcentagem, a ser mostrado no plot
    % saveName - Nome dos arquivos que conter�o um print do plot. Se o
    %            usu�rio n�o deseja salvar o plot num arquivo, deve inserir
    %            '' neste argumento.
    % fullScreen - Igual a 'full' se o usu�rio desejar o plot em tela
    %              inteira.
    % digital - Igual a 1 se o usu�rio quiser que os gr�ficos sejam
    %           plotados em degraus (digital no tempo) e 0 caso contr�rio

    % Instru��es de utiliza��o:
    % 1 - Esta fun��o d� um hold on autom�tico, ent�o se voc� cham�-la
    % utilizando o mesmo n�mero de figura 2 vezes, voc� ter� uma
    % sobreposi��o de plots no mesmo figure.
    % 2 - Caso voc� queira plotar v�rios gr�ficos no mesmo plot para depois
    % salvar em um arquivo, voc� deve inserir o nome do arquivo no
    % argumento saveName apenas no seu �ltimo plot. Caso contr�rio, voc�
    % salvar� uma figura que n�o ter� todos os seus gr�ficos ainda.
    % 3 - Caso voc� queira plotar em tela inteira, com v�rios gr�ficos no 
    % mesmo plot, voc� n�o precisa necessariamente inserir o par�metro 
    % 'full' em todos os seus plots. Basta inserir em um deles.
    
    % Verifica��o dos limites de X.
    if xMin > xMax
        display('Reveja os valores de xMin e xMax');
        return;
    end
    
    % Plot dos dados recebidos.
    figure(figureNumber); hold on;
    minIndex = round(xMin*length(x) + 1);
    maxIndex = round(xMax*length(x));
    x = x(minIndex:maxIndex);
    y = y(minIndex:maxIndex);
    
    if digital == 1
        xB = [];
        yB = [];
        for i = 1:length(x)-1
            xB = [xB linspace(x(i), x(i+1), 1000)];
            yB = [yB ones(1,1000)*y(i)];
        end

        x = xB;
        y = yB;
    end
    
    if isempty(plotConfig)
        plot(x,y);
    else
        plot(x,y,plotConfig);
    end
    
    %Configura��es de exibi��o.
    xlabel(lableX);
    ylabel(lableY);
    title(plotTitle);
    if strcmp(gridCte,'grid')
        grid on;
    end
    set(findall(gca, 'Type', 'Line'),'LineWidth',thickness);
    
    % Determina��o dos limites do gr�fico.
    sizeAxis = axis;
    sizeAxis(1) = min(x);
    sizeAxis(2) = max(x);
    if min(y) ~= max(y)
        sizeAxis(3) = min(y) - abs(max(y) - min(y))*0.1;
        sizeAxis(4) = max(y) + abs(max(y) - min(y))*0.1;
    end
    axis(sizeAxis);
    
    % Salvamento do plot em um arquivo .png e um em um arquivo .fig.
    if strcmp(fullScreen,'full')
        % Utilizado para maximizar a janela do plot
        set(gcf, 'Position', get(0, 'Screensize'));
    end
    if ~isempty(saveName)
        if exist('results','dir') == 0
            mkdir('results');
        end
        if exist('results\figs','dir') == 0
            mkdir('results\figs');
        end
        print(strcat('results\', saveName), '-dpng');
        savefig(strcat('results\figs\', saveName));
    end
    
end