package control;

import java.util.ArrayList;

import file.FileHandler;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Sound;
import utilities.Matrix;

public class STR {
	// Constantes para controle de execu��o
	public static final boolean debug = true;

	// Vari�veis para controle de execu��o
	private boolean estimation = false;
	private long lastComputeTime = 0;
	private long lastComputeCallTime = 0;
	private boolean firstCompute = true;
	public int error = 0;

	// Par�metros do modelo
	private double am1 = 0;
	private double am2 = 0;
	private double a0 = 0;

	// Inicializa��o de vari�veis utilizadas na simula��o da planta
	private double y_1 = 0; // y(t-1)
	private double y_2 = 0; // y(t-2)
	private double u = 0; // u(t)
	private double u_1 = 0; // u(t-1)
	private double u_2 = 0; // u(t-2)

	// Inicializa��o das vari�veis utilizadas na simula��o do modelo
	private double ym = 0; // ym(t)
	private double ym_1 = 0; // ym(t-1)
	private double ym_2 = 0; // ym(t-2)
	private double uc_1 = 0; // uc(t-1)
	private double uc_2 = 0; // uc(t-2)

	// Inicializa��o das vari�veis utilizadas na estima��o
	private double ye = 0;
	private double errEst;
	private Matrix fi;
	private Matrix P;
	private Matrix teta;

	private double[][] data = new double[4][1];
	private double pScalarAux; // pScalarAux = fi'*P*fi
	private double pScalar; // pScalar = (1/(1 + pScalarAux));
	private Matrix pTerm1; // Vari�vel utilizada no c�lculo do escalar pScalar1
	private Matrix pTerm2; // Vari�vel utilizada no c�lculo de P
	private Matrix fiT;
	private Matrix K;
	private Matrix yeMatrix;
	private Matrix tetaTerm;

	// Inicializa��o dos par�metros da planta
	private double a1 = 0;
	private double a2 = 0;
	private double b0 = 0;
	private double b1 = 0;

	// Inicializa��o dos par�metros do controlador
	private double s0;
	private double s1;
	private double r1;
	public double beta;
	private double MAX_U = 60;
	private double MIN_U = -MAX_U;
	private double T;

	// Par�metros para armazenamento dos par�metros do controlador
	private FileHandler file = new FileHandler("control.dat");
	private ArrayList<Double> parameters = new ArrayList<Double>();

	public STR() {
		P = Matrix.identity(4);
		P = P.times(1);
		fi = new Matrix(4, 1);
		double initialA1 = -1.13; // Valores iniciais para os par�metros estimados
		double initialA2 = 0.13;
		double initialB0 = -0.1;
		double initialB1 = 0.67;
		double tetaValues[][] = { {initialA1}, {initialA2}, {initialB0}, {initialB1} };
		teta = new Matrix(tetaValues); // Inicializa��o do vetor de par�metros
		load();
	}

	public void save() {
		parameters.add(a1);
		parameters.add(a2);
		parameters.add(b0);
		parameters.add(b1);
		parameters.add(am1);
		parameters.add(am2);
		parameters.add(T);
		file.saveInFile(parameters);

		if (debug) {
			LCD.clear();
			LCD.drawString("Saving...", 0, 0);
			LCD.drawString("a1 = " + a1, 0, 1);
			LCD.drawString("a2 = " + a2, 0, 2);
			LCD.drawString("b0 = " + b0, 0, 3);
			LCD.drawString("b1 = " + b1, 0, 4);
			LCD.drawString("am1 = " + am1, 0, 5);
			LCD.drawString("am2 = " + am2, 0, 6);
			LCD.drawString("T = " + T, 0, 7);
			Button.waitForAnyPress();
			while (Button.readButtons() != 0);
		}
	}

	public void load() {
		if (file.sizeFile() == 0) {
			LCD.clear();
			LCD.drawString("No file", 0, 0);
			Button.waitForAnyPress();
			while (Button.readButtons() != 0);
			return;
		}
		parameters = file.loadFromFile();
		a1 = parameters.get(0);
		a2 = parameters.get(1);
		b0 = parameters.get(2);
		b1 = parameters.get(3);
		am1 = parameters.get(4);
		am2 = parameters.get(5);
		T = parameters.get(6);
				
		if (debug) {
			LCD.clear();
			LCD.drawString("Loading...", 0, 0);
			LCD.drawString("a1 = " + a1, 0, 1);
			LCD.drawString("a2 = " + a2, 0, 2);
			LCD.drawString("b0 = " + b0, 0, 3);
			LCD.drawString("b1 = " + b1, 0, 4);
			LCD.drawString("am1 = " + am1, 0, 5);
			LCD.drawString("am2 = " + am2, 0, 6);
			LCD.drawString("T = " + T, 0, 7);
			Button.waitForAnyPress();
			while (Button.readButtons() != 0);
		}
	}
	
	public void reset() {
		a1 = 0;
		a2 = 0;
		b0 = 0;
		b1 = 0;
		
		if (debug) {
			LCD.clear();
			LCD.drawString("Reset...", 0, 0);
			LCD.drawString("a1 = " + a1, 0, 1);
			LCD.drawString("a2 = " + a2, 0, 2);
			LCD.drawString("b0 = " + b0, 0, 3);
			LCD.drawString("b1 = " + b1, 0, 4);
			Button.waitForAnyPress();
			while (Button.readButtons() != 0);
		}
	}
	
	public STR(double Mp, double ta, double T) {
		this();
		setControl(Mp, ta, T);
	}

	public STR(double Mp, double ta, double T, boolean estimation) {
		this(Mp, ta, T);
		setEstimation(estimation);
	}

	public void setModel(double am1, double am2, double T) {
		this.T = T;
		this.am1 = am1;
		this.am2 = am2;
	}
	
	public void setControl(double Mp, double ta, double T) {
		double qsiAux, qsiAuxPi;
		double qsi, wn;
		double a0, a1, a2;
		this.T = T * 1000;
		Mp = Mp / 100;
		qsiAux = Math.log(Mp);
		qsiAux = Math.pow(qsiAux, 2);
		qsiAuxPi = Math.pow(Math.PI, 2);
		qsi = Math.sqrt((qsiAux) / (qsiAux + qsiAuxPi));
		wn = (4) / (qsi * ta);
		a0 = 4 + 4 * T * qsi * wn + Math.pow(T * wn, 2);
		a1 = 2 * Math.pow(T * wn, 2) - 8;
		a2 = 4 - 4 * T * qsi * wn + Math.pow(T * wn, 2);
		am1 = a1 / a0;
		am2 = a2 / a0;

		if (debug) {
			LCD.clear();
			LCD.drawString("Model", 0, 0);
			LCD.drawString("am1 = " + am1, 0, 0);
			LCD.drawString("am2 = " + am2, 0, 1);
			LCD.drawString("T = " + this.T, 0, 2);
			Button.waitForAnyPress();
			while (Button.readButtons() != 0)
				;
		}
	}

	public double compute(double y, double uc) {
		// Reiniciando o erro; 
		error = 0;
		
		// Avaliando tempo de amostragem
		if(firstCompute) {
			firstCompute = false;
			lastComputeCallTime = System.currentTimeMillis();
		}
		if(System.currentTimeMillis() - lastComputeCallTime > T) {
			LCD.clear();
			LCD.drawString("increase T", 0, 0);
			Sound.playTone(1000, 1);
			error = 1;
			return 0;
		}
		lastComputeCallTime = System.currentTimeMillis();
		
		if (System.currentTimeMillis() - lastComputeTime >= T) {
			lastComputeTime = System.currentTimeMillis();
			if (estimation) {
				// Atualiza��o dos par�metros do modelo
				data[0][0] = -y_1;
				data[1][0] = -y_2;
				data[2][0] = u_1;
				data[3][0] = u_2;
				fi.setMatrix(data); // Setando fi
				fiT = fi.transpose(); // Obtendo fi'

				// pScalarAux = fi'*P*fi
				pTerm1 = fiT.times(P);
				pTerm1 = pTerm1.times(fi);
				pScalarAux = pTerm1.getElement(0, 0); // Obtendo o escalar restante das opera��es acima

				// pScalar = (1/(1 + pScalarAux));
				pScalar = 1 / (1 + pScalarAux);

				// P*fi*pScalar*fi'*P
				pTerm2 = P.times(fi);
				pTerm2 = pTerm2.times(pScalar);
				pTerm2 = pTerm2.times(fiT);
				pTerm2 = pTerm2.times(P);
				P = P.minus(pTerm2);

				// K = P*fi;
				K = P.times(fi);

				// ye = fi'*teta;
				yeMatrix = fiT.times(teta);
				ye = yeMatrix.getElement(0, 0);
				errEst = y - ye;

				// tetaTerm = K*errEst;
				tetaTerm = K.times(errEst);

				// teta = teta + tetaTerm;
				teta = teta.plus(tetaTerm);

				// Atualizando par�metros da planta
				a1 = teta.getElement(0, 0);
				a2 = teta.getElement(1, 0);
				b0 = teta.getElement(2, 0);
				b1 = teta.getElement(3, 0);
			}

			// Atualizando par�metros do controlador
			r1 = (am2 + am1 * a0 - a2 - (b0 / b1) * am2 * a0 - (b1 / b0) * (am1 - a1 + a0))
					/ (a1 - (b0 / b1) * a2 - (b1 / b0));
			s0 = (am1 + a0 - a1 - r1) / b0;
			s1 = (am2 * a0 - a2 * r1) / b1;
			beta = (1 + am1 + am2) / (b0 + b1);

			// Computando sa�da do modelo
			ym = -am1 * ym_1 - am2 * ym_2 + beta * b0 * uc_1 + beta * b1 * uc_2;

			// uc = ucAmp * ucSig;
			u = -r1 * u_1 + beta * uc + beta * a0 * uc_1 - s0 * y - s1 * y_1;

			// Corrigindo a��o de controle
			if (u > MAX_U)
				u = MAX_U;
			if (u < MIN_U)
				u = MIN_U;

			// Atualizando vari�veis que armanezam dados de itera��es passadas
			y_2 = y_1;
			y_1 = y;
			u_2 = u_1;
			u_1 = u;
			ym_2 = ym_1;
			ym_1 = ym;
			uc_2 = uc_1;
			uc_1 = uc;
		}
		return u;
	}

	public double getModelOutput() {
		return ym;
	}

	public void setEstimation(boolean estimation) {
		this.estimation = estimation;
	}

	public static void main(String args[]) {
		STR str = new STR(1.0, 2.0, 0.05);
		str.load();
	}
}
