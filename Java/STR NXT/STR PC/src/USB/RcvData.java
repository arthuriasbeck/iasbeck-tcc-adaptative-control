package USB;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Locale;

public class RcvData {

	// Vari�veis para debug
	public boolean debug = false;
	private String debugMsg = "4.564a1.0b2.0c3.0d2.144453e2.0";
	
	// Vari�veis para recebimento e tratamento das mensagens
	char[] dataIndex = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
			's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
	USBCommPC usbComm = new USBCommPC("rcvUSB.txt");
	String rcvMsg = "";
	int startIndex;
	int endIndex;
	String toFile;
	ArrayList<String> msgData = new ArrayList<String>();
	
	// Mensagens para salvar os dados recebidos num arquivo
	File f = null;
	FileWriter w = null;
	BufferedWriter bw = null;

	private void setFile() {
		Locale locale = new Locale("pt", "BR");
		GregorianCalendar calendar = new GregorianCalendar();
		SimpleDateFormat formatador = new SimpleDateFormat("dd' de 'MMMMM' de 'yyyy' - 'HH'h'mm' '", locale);
		String currentTime = formatador.format(calendar.getTime()).trim();
		System.out.println(currentTime);

		String fileName = "results/" + currentTime + ".txt";
		
		try {
			f = new File(fileName);
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
		} catch (IOException e) {
			System.out.println("Erro na cria��o do arquivo");
			return;
		}
	}
	
	public void toFile() {
		try {
			bw.write(toFile);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			System.out.println("Erro na escrita do arquivo");
			return;
		}

		System.out.println(toFile);
	}
	
	public void endComm() {
		System.out.println("Fim da execu��o");
		try {
			bw.close();
		} catch (IOException e) {
			System.out.println("Erro no fechamento do arquivo");
			return;
		}

		if (!debug) {
			usbComm.close();
		}
	}
	
	public void rcv() {
		// Iniciando arquivo para armazenar os dados recebidos
		setFile();
		
		// Iniciando conex�o USB
		if (!debug) {
			usbComm.connect();
		}
		
		// Recebendo as mensagens
		while (true) {
			if (!debug) {
				rcvMsg = usbComm.receiveString();
			} else {
				rcvMsg = debugMsg;
			}
			if (rcvMsg.compareTo("*") == 0) {
				break;
			}

			// Separando cara um dos dados continos na mensagem
			startIndex = 0;
			msgData.clear();
			for (int i = 0; i < dataIndex.length; i++) {
				endIndex = rcvMsg.indexOf(dataIndex[i]);
				if (endIndex == -1) {
					msgData.add(rcvMsg.substring(startIndex, rcvMsg.length()));
					break;
				}
				msgData.add(rcvMsg.substring(startIndex, endIndex));
				startIndex = endIndex + 1;
			}

			// Escrevendo os dados recebidos no arquivos
			toFile = "";
			for (int i = 0; i < msgData.size(); i++) {
				toFile += msgData.get(i) + "\t";
			}
			toFile();
			
			if (debug) {
				break; // No debug nos interessa executar o la�o uma �nica vez
			}
		}
		endComm();
	}

	public static void main(String args[]) {
		RcvData rcvData = new RcvData();
		rcvData.rcv();
	}
}
