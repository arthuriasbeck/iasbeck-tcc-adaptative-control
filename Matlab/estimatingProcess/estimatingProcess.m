% Este programa tem como finalizada simular o funcionamento do seguidor. A equa��o diferen�a utilizada aqui foi obtida a partir de
% experimentos reais utilizando o seguidor e aqui ser� utilizada
% a mesma entrada que foi utilizada nos experimentos de aquisi��o. Al�m
% disso, ser� realizada uma estima��o que executar� iterativamente tentando
% se ajustar aos valores de y gerados pela equa��o diferen�a obtida
% experimentalmente. Veja que � exatamente essa estima��o que acontecer�
% quando o STR for implementado no rob�, com a diferen�a de que, ao inv�s
% dos valores de y serem gerados por uma equa��o diferen�a, eles vir�o
% diretamente da aquisi��o dos sensores do rob�. A simula��o aqui realizada
% representa bem a estima��o que acontecer� na realidade.

addpath('libraries');
addpath('../libraries/PlotData')

clear; clc; close all;

% Par�metros para execu��o
T = 0.05;   % Tempo de amostragem
tSim = 10;  % Tempo de simula��o
tInput = 1; % Tempo para que ocorra uma mudan�a na entrada do sistema
uAmp = 10;  % Determina a amplitude da entrada degrau
uSig = 0;   % Determina se o degrau ser� positivo ou negativo

% Inicializa��o de vari�veis para armazenamento de dados
uArray = zeros(1,1);        % Vetor que armazena os valores de entrada
yArray = zeros(1,1);        % Vetor que armazena os valores de sa�da
yeArray = zeros(1,1);       % Vetor que armazena os valores de estima��o
a1Array = zeros(1,1);       % Vetor que armazena os valores de a1
a2Array = zeros(1,1);       % Vetor que armazena os valores de a2
b0Array = zeros(1,1);       % Vetor que armazena os valores de b0
b1Array = zeros(1,1);       % Vetor que armazena os valores de b1
errEstArray = zeros(1,1);   % Vetor que armazena os valores do erro de estima��o

% Inicializa��o de vari�veis utilizadas na simula��o da planta
y = 0;      % y(t)
y_1 = 0;    % y(t-1)
y_2 = 0;    % y(t-2)
u = 0;      % u(t)
u_1 = 0;    % u(t-1)
u_2 = 0;    % u(t-2)

% Inicializa��o das vari�veis utilizadas na estima��o
ye = 0;                     % Sa�da estimada pelo m�nimos quadrados
fi = [-y_1 -y_2 u_1 u_2];   % Vetor de vari�veis conhecidas
parNum = length(fi);        % Quantidade de par�metros a serem determinados
P = rand(parNum);           % Matriz de corre��o
teta = rand(parNum,1);      % Vetor de par�metros

% Vari�veis para controle de execu��o
inputData = load('data/inputData.txt');
numLoops = length(inputData);  % Quantidade de itera��es na simula��o

for loops = 1:numLoops
    % Computando entradas e sa�das
    y = 1.0002*y_1 + 0.0008*y_2 - 0.0244*u_1 + 0.1503*u_2;
    u = inputData(loops);
    
    % Atualiza��o dos par�metros do modelo
    fi = [-y_1 -y_2 u_1 u_2]';
    P = P - P*fi*(1/(1 + fi'*P*fi))*fi'*P;
    K = P*fi;
    ye = fi'*teta;
    errEst = y - ye;
    teta = teta + K*errEst;
    
    % Atualizando par�metros da planta/controlador
    a1 = teta(1);
    a2 = teta(2);
    b0 = teta(3);
    b1 = teta(4);
    
    % Armazenando dados de execu��o
    uArray(loops) = u;
    yArray(loops) = y;
    yeArray(loops) = ye;
    a1Array(loops) = a1;
    a2Array(loops) = a2;
    b0Array(loops) = b0;
    b1Array(loops) = b1;
    errEstArray(loops) = errEst;
    
    % Atualizando vari�veis que armanezam dados de itera��es passadas
    y_2 = y_1;
    y_1 = y;
    u_2 = u_1;
    u_1 = u;
end

% Mostrando os resultados da simula��o
U = uArray;
Y = yArray;
YE = yeArray;
E = errEstArray;
A1 = a1Array;
A2 = a2Array;
B0 = b0Array;
B1 = b1Array;
plotResults(numLoops, T, U, Y, YE, E, A1, A2, B0, B1);
