package utilities;

/**
 * Classe que implementa o m�todo dos m�nimos quadrados para um modelo linear de
 * segunda ordem e delay de 1 (ou seja, a entrada atual n�o interefere na sa�da
 * atual). O algoritmo que inpirou o desenvolvimento desta classe pode ser
 * encontrado abaixo
 * 
 * @author Arthur Iasbeck
 *
 */

// % Atualiza��o dos par�metros do modelo
// fi = [-y_1 -y_2 u_1 u_2]';
// P = P - P*fi*pScalar*fi'*P;
// pScalarAux = fi'*P*fi;
// pScalar = (1/(1 + pScalarAux));
// K = P*fi;
// ye = fi'*teta;
// errEst = y - ye;
// tetaTerm = K*errEst;
// teta = teta + tetaTerm;

// % Atualiza��o das entradas e sa�das do processo
// y_2 = y_1;
// y_1 = y;
// u_2 = u_1;
// u_1 = u;

public class LeastSquares {
	public static final boolean DEBUG = false;
	Matrix fi, P, teta;
	double ye, errEst;
	double y_2, y_1, u_2, u_1;

	/**
	 * Construtor da classe.
	 */
	public LeastSquares() {
		P = Matrix.random(4, 4);
		if (DEBUG) {
			System.out.println("\nP = ");
			P.show();
		}

		fi = new Matrix(4, 1);
		if (DEBUG) {
			System.out.println("\nfi = ");
			fi.show();
		}

		teta = Matrix.random(4, 1);
		if (DEBUG) {
			System.out.println("\nteta = ");
			teta.show();
		}
	}

	/**
	 * M�todo respons�vel por computar a sa�da do modelo e atualizar os par�metros
	 * do mesmo. Recebe como par�metros a entrada e a sa�da atuais do modelo.
	 * 
	 * @param y
	 * @param u
	 * @return
	 */
	public double compute(double y, double u) {
		double[][] data = new double[4][1];
		data[0][0] = -y_1;
		data[1][0] = -y_2;
		data[2][0] = u_1;
		data[3][0] = u_2;

		double pScalarAux; // pScalarAux = fi'*P*fi
		double pScalar; // pScalar = (1/(1 + pScalarAux));
		Matrix pTerm1; // Vari�vel utilizada no c�lculo do escalar pScalar1
		Matrix pTerm2; // Vari�vel utilizada no c�lculo de P
		Matrix fiT;
		Matrix K;
		Matrix yeMatrix;
		Matrix tetaTerm;

		fi.setMatrix(data); // Setando fi
		if (DEBUG) {
			System.out.println("\nfi = ");
			fi.show();
		}

		fiT = fi.transpose(); // Obtendo fi'
		if (DEBUG) {
			System.out.println("\nfi' = ");
			fiT.show();
		}

		// pScalarAux = fi'*P*fi
		pTerm1 = fiT.times(P);
		if (DEBUG) {
			System.out.println("\nfi'*P = ");
			pTerm1.show();
		}

		pTerm1 = pTerm1.times(fi);
		if (DEBUG) {
			System.out.println("\nfi'*P*fi = ");
			pTerm1.show();
		}

		pScalarAux = pTerm1.getElement(0, 0); // Obtendo o escalar restante das opera��es acima
		if (DEBUG) {
			System.out.println("\nfi'*P*fi = " + pScalarAux);
		}

		// pScalar = (1/(1 + pScalarAux));
		pScalar = 1 / (1 + pScalarAux);
		if (DEBUG) {
			System.out.println("\n1/(1 + fi'*P*fi) = " + pScalar);
		}

		// P*fi*pScalar*fi'*P
		pTerm2 = P.times(fi);
		if (DEBUG) {
			System.out.println("\nP*fi = ");
			pTerm2.show();
		}

		pTerm2 = pTerm2.times(pScalar);
		if (DEBUG) {
			System.out.println("\nP*fi*1/(1 + fi'*P*fi) = ");
			pTerm2.show();
		}

		pTerm2 = pTerm2.times(fiT);
		if (DEBUG) {
			System.out.println("\nP*fi*1/(1 + fi'*P*fi)*fi' = ");
			pTerm2.show();
		}

		pTerm2 = pTerm2.times(P);
		if (DEBUG) {
			System.out.println("\nP*fi*1/(1 + fi'*P*fi)*fi'*P = ");
			pTerm2.show();
		}

		P = P.minus(pTerm2);
		if (DEBUG) {
			System.out.println("\nP = P - P*fi*1/(1 + fi'*P*fi)*fi'*P = ");
			P.show();
		}

		// K = P*fi;
		K = P.times(fi);
		if (DEBUG) {
			System.out.println("\nK = ");
			K.show();
		}

		// ye = fi'*teta;
		yeMatrix = fiT.times(teta);
		if (DEBUG) {
			System.out.println("\nye = ");
			yeMatrix.show();
		}

		ye = yeMatrix.getElement(0, 0);
		if (DEBUG) {
			System.out.println("\nye = " + ye);
		}

		errEst = y - ye;
		if (DEBUG) {
			System.out.println("\nerrEst = " + errEst);
		}

		// tetaTerm = K*errEst;
		tetaTerm = K.times(errEst);
		if (DEBUG) {
			System.out.println("\nK*errEst = ");
			tetaTerm.show();
		}

		// teta = teta + tetaTerm;
		teta = teta.plus(tetaTerm);
		if (DEBUG) {
			System.out.println("\nteta = teta +  K*errEst = ");
			teta.show();
		}

		y_2 = y_1;
		y_1 = y;
		u_2 = u_1;
		u_1 = u;

		return ye;
	}

	public double getError() {
		return errEst;
	}

	public double[] getPar() {
		double[] par = new double[4];
		par[0] = teta.getElement(0, 0);
		par[1] = teta.getElement(1, 0);
		par[2] = teta.getElement(2, 0);
		par[3] = teta.getElement(3, 0);
		return par;
	}
	
	public static void main(String args[]) {
		LeastSquares leastSquares = new LeastSquares();
		leastSquares.compute(1, 2);
		// System.out.println("\ngetError() = " + leastSquares.getError());
	}
}
