package classTest;

import utilities.LeastSquares;

public class LeastSquaresTest {
	public static void main(String args[]) throws InterruptedException {
		double y = 0, y_1 = 0, y_2 = 0, u_1 = 0, u_2 = 0, e = 0, ye = 0;
		double u = 0;
		int countChangeU = 0;
		LeastSquares leastSquares = new LeastSquares();

		for (int i = 0; i < 201; i++) {
			y = 1.0002 * y_1 + 0.0008 * y_2 - 0.0244 * u_1 + 0.1503 * u_2;
			ye = leastSquares.compute(y, u);
			
			countChangeU++;
			if(countChangeU == 20) {
				if(u == 0) u = 10;
				else if(u == 10) u = -10;
				else if(u == -10) u = 10;
				countChangeU = 0;
			}
			
			e = leastSquares.getError();
			System.out.println(y + "\t" + ye + "\t" + e + "\t" + u);
			y_2 = y_1;
			y_1 = y;
			u_2 = u_1;
			u_1 = u;
		}
		
		double[] par = leastSquares.getPar();
		System.out.println("\nparameters = ");
		System.out.println("a1 = " + par[0]);
		System.out.println("a2 = " + par[1]);
		System.out.println("b0 = " + par[2]);
		System.out.println("b1 = " + par[3]);
		
	}
}
