package USB;

import java.util.ArrayList;

import File.FileHandler;
import lejos.nxt.Button;
import lejos.nxt.LCD;

public class SendUSB {

	public void sendU() {
		FileHandler fileU = new FileHandler("u.dat"); // Arquivo para as entradas
		ArrayList<Double> uData = new ArrayList<Double>(); // ArrayList de entradas
		uData = fileU.loadFromFile();

		USBComm usb = new USBComm();
		usb.connect();
		for (int i = 0; i < uData.size(); i++) {
			usb.send(uData.get(i));
		}
		usb.send(-1.0);

		LCD.clear();
		LCD.drawString("Envio de u \nfinalizado. \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

	public void sendY() {
		FileHandler fileY = new FileHandler("y.dat"); // Arquivo para as sa�das
		ArrayList<Double> yData = new ArrayList<Double>(); // ArrayList de sa�das
		yData = fileY.loadFromFile();

		USBComm usb = new USBComm();
		usb.connect();
		for (int i = 0; i < yData.size(); i++) {
			usb.send(yData.get(i));
		}
		usb.send(-1.0);

		LCD.clear();
		LCD.drawString("Envio de y \nfinalizado. \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

	public static void main(String args[]) {
		SendUSB sendUSB = new SendUSB();
		sendUSB.sendU();
		// sendUSB.sendY();

	}
}
