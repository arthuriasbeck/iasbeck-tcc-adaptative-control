package USB;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import lejos.pc.comm.NXTConnector;

public class USBCommPC {

	public static final boolean DEBUG = false;
	public static DataOutputStream outData;
	public static DataInputStream inData;
	public static NXTConnector link;
	public File f;
	public FileWriter w;
	public BufferedWriter bw;
	public boolean firstLoop = true;
	public long time = 0;

	static int msgCount = 0;

	USBCommPC(String fileName) {
		try {
			f = new File(fileName);
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
		} catch (IOException e) {
			System.out.println("Problemas com arquivo");
		}
	}

	USBCommPC() {

	}

	/*
	 * Esta fun��o realiza a leitura de uma string atrav�s da leitura individual de
	 * cada um dos bytes da mensagem at� que seja encontrado o byte que delimita o
	 * fim da mensagem, neste caso, o 'X'. O caractere X � ent�o removido da
	 * mensagem obtida e o resultado � retornado. O 'X' n�o � percebido pelo usu�rio
	 * nem no envio e nem no recebimento das mensagens.
	 */
	public String receiveString() {
		int charCount = 0;
		if (DEBUG) {
			System.out.println("\nRealizando a leitura da mensagem " + msgCount++);
		}

		String readMsg = "";
		char readValue;
		while (true) {
			try {
				readValue = inData.readChar();
				readMsg += readValue;
				if (DEBUG) {
					System.out.println("Leitura do char " + charCount++ + " = " + readValue);
				}
				if (readValue == 'x') {
					readMsg = readMsg.substring(0, (readMsg.length() - 1));
					if (DEBUG) {
						System.out.println("Mensagem obtida = " + readMsg);
					}
					try {
						bw.write("" + readMsg);
						bw.newLine();
						bw.flush();
					} catch (IOException e) {
						System.out.println("Problemas com arquivo");
					}
					return readMsg;
				}
			} catch (IOException e) {
				System.out.println("Problema no Receive");
			}
		}
	}

	public void connect() {
		link = new NXTConnector();

		if (!link.connectTo("usb://")) {
			System.out.println("\nNo NXT find using USB");
			return;
		}

		outData = new DataOutputStream(link.getOutputStream());
		inData = new DataInputStream(link.getInputStream());
		System.out.println("\nNXT is Connected");
	}

	public void close() {
		try {
			outData.close();
			inData.close();
			link.close();
		} catch (IOException e) {
			System.out.println("Erro no fechamento da comunica��o");
		}
	}

	/*
	 * Fun��o utilizada no teste da classe USBComm.
	 */
	public static void main(String args[]) {
		// Teste para recebimento de uma string
		String readValue = "";
		USBCommPC usbComm = new USBCommPC("y.txt");
		usbComm.connect();
		while (readValue.compareTo("*") != 0) {
			readValue = usbComm.receiveString();
			System.out.println("readValue = " + readValue);
		}
		System.out.println("Fim da execu��o");
		usbComm.close();
	}
}
