package USB;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import lejos.pc.comm.NXTConnector;

public class USBCommPC {
	
	public static final boolean DEBUG = false;
	
	USBCommPC(String fileName) {
		try {
			f = new File(fileName);
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
		} catch (IOException e) {
			System.out.println("Problemas com arquivo");
		}
	}

	USBCommPC() {

	}

	public static DataOutputStream outData;
	public static DataInputStream inData;
	public static NXTConnector link;
	public File f;
	public FileWriter w;
	public BufferedWriter bw;
	public boolean firstLoop = true;
	public long time = 0;

	static int msgCount = 0;

	public void send(double value) {
		try {
			outData.writeFloat((float) value);
			outData.flush();
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}

	/**
	 * Fun��o utilizada na obten��o da planta de um seguidor de linha. Os valores
	 * recebidos s�o a entrada e a sa�da da planta.
	 * 
	 * @param value1
	 * @param value2
	 */
	public void send(double value1, double value2) {
		try {
			outData.writeFloat((float) value1);
			outData.writeFloat((float) value2);
			outData.flush();
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}

	public void send(int value) {
		try {
			outData.writeInt(value);
			outData.flush();
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}

	public double receiveDouble() {
		double read = 0;
		try {
			read = inData.readFloat();
			if (read == -1) {
				return read;
			}
		} catch (IOException e) {
			System.out.println("Problema no Receive");
		}
		System.out.println(read);

		try {
			bw.write("" + read);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			System.out.println("Problemas com arquivo");
		}
		return read;
	}

	/**
	 * Esta fun��o realiza a leitura de uma string atrav�s da leitura individual de
	 * cada um dos bytes da mensagem at� que seja encontrado o byte que delimita o
	 * fim da mensagem, neste caso, o 'X'. O caractere X � ent�o removido da
	 * mensagem obtida e o resultado � retornado. O 'X' n�o � percebido pelo usu�rio
	 * nem no envio e nem no recebimento das mensagens.
	 * 
	 * @return
	 */
	public String receiveString() {
		int charCount = 0;
		if(DEBUG) {
			System.out.println("\nRealizando a leitura da mensagem " + msgCount++);
		}
		
		String readMsg = "";
		char readValue;
		while (true) {
			try {
				readValue = inData.readChar();
				readMsg += readValue;
				if(DEBUG) {
					System.out.println("Leitura do char " + charCount++ + " = " + readValue);
				}
				if (readValue == 'X') {
					readMsg = readMsg.substring(0, (readMsg.length() - 1));
					if(DEBUG) {
						System.out.println("Mensagem obtida = " + readMsg);
					}
					try {
						bw.write("" + readMsg);
						bw.newLine();
						bw.flush();
					} catch (IOException e) {
						System.out.println("Problemas com arquivo");
					}
					return readMsg;
				}
			} catch (IOException e) {
				System.out.println("Problema no Receive");
			}
		}
	}

	public int receiveInt() {
		int read = 0;
		try {
			read = inData.readInt();
		} catch (IOException e) {
			System.out.println("Problema no Receive");
		}
		System.out.println(read);

		try {
			bw.write("" + read);
			bw.write("\t");
			if (firstLoop) {
				time = System.currentTimeMillis();
				firstLoop = false;
			}
			bw.write("" + (System.currentTimeMillis() - time));
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			System.out.println("Problemas com arquivo");
		}
		return read;
	}

	public void connect() {
		link = new NXTConnector();

		if (!link.connectTo("usb://")) {
			System.out.println("\nNo NXT find using USB");
		}

		outData = new DataOutputStream(link.getOutputStream());
		inData = new DataInputStream(link.getInputStream());
		System.out.println("\nNXT is Connected");
	}

	public void close() {
		try {
			outData.close();
			inData.close();
			link.close();
		} catch (IOException e) {
			System.out.println("Erro no fechamento da comunica��o");
		}
	}

	/**
	 * Fun��o utilizada no teste da classe USBComm.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		/*
		 * Teste para o envio de um double
		 */
		// double readValue = 0;
		// USBCommPC usbComm = new USBCommPC("y.txt");
		// usbComm.connect();
		// while (readValue != -1.0) {
		// readValue = usbComm.receiveDouble();
		// }
		// System.out.println("Fim da execu��o");
		// usbComm.close();

		/*
		 * Teste para o envio de uma string
		 */
		String readValue = "";
		USBCommPC usbComm = new USBCommPC("y.txt");
		usbComm.connect();
		/*
		 * N�o podemos usar == neste caso pq a String n�o foi construida pela classe
		 * String mas sim por um conjunto de caracteres, o que significa que ela n�o
		 * est� na base de dados do Java, onde ele armazena as Strings. A String s�o
		 * armazenadas desta forma para que n�o sejam repetidos na mem�ria dois
		 * conjuntos de caracteres diferentes. Assim sendo, a String criada por n�s, que
		 * n�o foi criada atrav�s do uso de m�todos da classe String, n�o se encontra na
		 * mem�ria e n�o podemos utilizar a compara��o do tipo ==.
		 */
		while (readValue.compareTo("*") != 0) {
			readValue = usbComm.receiveString();
			System.out.println("readValue = " + readValue);	
		}
		System.out.println("Fim da execu��o");
		usbComm.close();
	}
}
