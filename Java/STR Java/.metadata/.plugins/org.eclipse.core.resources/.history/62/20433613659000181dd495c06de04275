package utilities;

/**
 * Classe utilizada na manipulação de matrizes. Pode ser acessada em
 * (https://introcs.cs.princeton.edu/java/95linear/Matrix.java.html). Aqui foram
 * apenas realizadas algumas modificações na função show() para que ela
 * mostrasse as matrizes no LCD do CLP Lego.
 * 
 * @author Robert Sedgewick e Kevin Wayne
 *
 */

final public class Matrix {
	private final int M; // number of rows
	private final int N; // number of columns
	private double[][] data; // M-by-N array

	// create M-by-N matrix of 0's
	public Matrix(int M, int N) {
		this.M = M;
		this.N = N;
		data = new double[M][N];
	}

	// create matrix based on 2d array
	public Matrix(double[][] data) {
		M = data.length;
		N = data[0].length;
		this.data = new double[M][N];
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				this.data[i][j] = data[i][j];
	}

	// create and return a random M-by-N matrix with values between 0 and 1
	public static Matrix random(int M, int N) {
		Matrix A = new Matrix(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				A.data[i][j] = Math.random();
		return A;
	}

	// create and return the N-by-N identity matrix
	public static Matrix identity(int N) {
		Matrix I = new Matrix(N, N);
		for (int i = 0; i < N; i++)
			I.data[i][i] = 1;
		return I;
	}

	// create and return the transpose of the invoking matrix
	public Matrix transpose() {
		Matrix A = new Matrix(N, M);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				A.data[j][i] = this.data[i][j];
		return A;
	}

	// return C = A + B
	public Matrix plus(Matrix B) {
		Matrix A = this;
		if (B.M != A.M || B.N != A.N)
			throw new RuntimeException("Illegal matrix dimensions.");
		Matrix C = new Matrix(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				C.data[i][j] = A.data[i][j] + B.data[i][j];
		return C;
	}

	// return C = A - B
	public Matrix minus(Matrix B) {
		Matrix A = this;
		if (B.M != A.M || B.N != A.N)
			throw new RuntimeException("Illegal matrix dimensions.");
		Matrix C = new Matrix(M, N);
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				C.data[i][j] = A.data[i][j] - B.data[i][j];
		return C;
	}

	// does A = B exactly?
	public boolean eq(Matrix B) {
		Matrix A = this;
		if (B.M != A.M || B.N != A.N)
			throw new RuntimeException("Illegal matrix dimensions.");
		for (int i = 0; i < M; i++)
			for (int j = 0; j < N; j++)
				if (A.data[i][j] != B.data[i][j])
					return false;
		return true;
	}

	// return C = A * B
	public Matrix times(Matrix B) {
		Matrix A = this;
		if (A.N != B.M)
			throw new RuntimeException("Illegal matrix dimensions.");
		Matrix C = new Matrix(A.M, B.N);
		for (int i = 0; i < C.M; i++)
			for (int j = 0; j < C.N; j++)
				for (int k = 0; k < A.N; k++)
					C.data[i][j] += (A.data[i][k] * B.data[k][j]);
		return C;
	}

	// print matrix to LCD
	public void show() {
		String currentRow = "";
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				currentRow += data[i][j] + " ";
			}
			System.out.println(currentRow);
		}
	}
	
	// set data to matrix 
	public void setMatrix(double data[][]) {
		int numRows = data[0].length;
		int numCols = data.length;
		if (M != numRows || N != numCols) {
			System.out.println("Error in setMatrix");
			return;
		}
		this.data = data;
	}

	// test client
	public static void main(String[] args) {
		double dataA[][] = { { 1, 2, 3 }, { 3, 4, 5 }, { 5, 6, 7 } };
		Matrix A = new Matrix(dataA);
		Matrix B = new Matrix(3, 3);
		A.show();
		B = A.transpose();
		B.show();
		B = B.times(B);
		B.show();
		double newData[][] = { { -1, -2, -3 }, { -3, -4, -5 }, { -5, -6, -7 } }; 
		A.setMatrix(newData);
		A.show();
	}
}
