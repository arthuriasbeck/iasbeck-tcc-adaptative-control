package Control;

/**
 * Nesta classe est�o contidos os m�todos respons�veis pela implementa��o de um
 * controlador do tipo PID. Para o nome das vari�veis s�o utilizadas as
 * seguintes nota��es
 * 
 * y = y(n) y_1 = y(n-1)
 * 
 * x -> Entradas y -> Sa�das
 * 
 * @author Iasbeck
 *
 */

public class PID {

	private double ref = 0;
	private double error = 0;
	private double error_1 = 0;
	private double ierror = 0;
	private double derror = 0;

	private double kp = 0;
	private double ki = 0;
	private double kd = 0;

	public PID(double ref, double kp, double ki, double kd) {
		this.ref = ref;
		this.kp = kp;
		this.kd = kd;
		this.ki = ki;
	}

	/**
	 * Fun��o respons�vel por computar o PID. Note que ela n�o leva em considera��o
	 * o tempo de amostragem, aqui embutido nos par�metros do controlador (kp, ki e
	 * kd). Isso significa que o tempo de amostragem � considerado constante.
	 * 
	 * @param y_1
	 */

	public double compute(double y_1) {
		double p, i, d;
		double u;
		error = ref - y_1;
		derror = error - error_1;
		ierror += error;

		/*
		 * Caso o erro mude de sinal, ou seja, a vari�vel controlada alcanse a
		 * refer�ncia e a ultrapasse, a somat�ria do erro � zerada para que n�o con
		 * tribua que a vari�vel controlada se afaste mais e mais da refer�ncia depois
		 * que j� a tenha alcan�ado.
		 */
		if (error * error_1 < 0) {
			ierror = 0;
		}
		
		p = kp*error;
		i = ki*ierror;
		d = kd*derror;
		
		u = p + i + d;
		return u;
	}

	public static void main(String[] args) {
//		PID pid = new PID(300, 10, 0, 0);
	}

}
