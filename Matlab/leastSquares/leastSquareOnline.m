% Este c�digo mostra a implementa��o do RLS a um conjunto de dados gerado 
% por uma rela��o polinimial. Os dados s�o gerados a medida que passam as
% itera��es e o RLS deve ajustar os par�metros de um dado modelo de forma 
% que este se aproxime cada vez mais dos dados gerados.

clear;
clc;
close all;

nData = 300;

u = rand;
fi = [1 u u^2]';

arrayTeta = zeros(3,1);
arrayY = zeros(1,1);
arrayMod = zeros(1,1);
arrayErr = zeros(1,1);

P = rand(length(fi));
teta = rand(3,1);

% Modelo 
% y(i) = b0 + b1u + b2u�

b0 = 1;
b1 = 0.45;
b2 = 0.1;

U = 1:nData;

for i = 1:nData
    u = rand*10;
    y = b0 + b1*u + b2*u^2;
    fi = [1 u u^2]';
    P = P - P*fi*(1/(1 + fi'*P*fi))*fi'*P;
    K = P*fi;
    err = y - fi'*teta;
    teta = teta + K*err;

    arrayTeta(:,i) = teta;
    arrayY(i) = y;
    arrayMod(i) = fi'*teta;
    arrayErr(i) = err;
end

%%
% Plotando resultados
x = 1:nData;
lx = 'Itera��es';
ly = 'y';
t = 'Compara��o entre a sa�da real e a sa�da estimada';

y = arrayY;
completeSubPlot(1,2,1,1,x,y,'',1.5,lx,ly,'','grid',0,0.1,'','');

y = arrayMod;
completeSubPlot(1,2,1,1,x,y,'ro',1.5,lx,ly,t,'grid',0,0.1,'','');
legend('Dado real','Dado estimado');

y = arrayY;
completeSubPlot(1,2,1,2,x,y,'',1.5,lx,ly,'','grid',0.9,1,'','');

y = arrayMod;
completeSubPlot(1,2,1,2,x,y,'ro',1.5,lx,ly,'','grid',0.9,1,'','');
legend('Dado real','Dado estimado');
completeSubPlot(1,2,1,2,x,y,'ro',1.5,lx,ly,'','grid',0.9,1,'output','');

% Plotando erro
x = 1:nData;
y = arrayErr;
lx = 'Itera��es';
ly = 'Erro';
t = 'Erro de estima��o ao longo das itera��es';
completeSubPlot(2,2,1,1,x,y,'',1.5,lx,ly,t,'grid',0,1,'','');
y = zeros(1,nData);
completeSubPlot(2,2,1,1,x,y,'--k',1.5,lx,ly,t,'grid',0,1,'err','');

% Plotando par�metros
x = 1:nData;
y = arrayTeta(1,:);
lx = 'Itera��es';
ly = 'b0';
t = 'Valores dos par�metros do modelo';
completeSubPlot(3,2,1,1,x,y,'',1.5,lx,ly,'','grid',0,1,'','');
y = ones(1,nData)*b0;
completeSubPlot(3,2,1,1,x,y,'--k',1.5,lx,ly,t,'grid',0,1,'','');

y = arrayTeta(2,:);
ly = 'b1';
completeSubPlot(3,2,1,2,x,y,'',1.5,lx,ly,'','grid',0,1,'','');
y = ones(1,nData)*b1;
completeSubPlot(3,2,1,2,x,y,'--k',1.5,lx,ly,'','grid',0,1,'b0_b1','');

y = arrayTeta(3,:);
ly = 'b2';
completeSubPlot(4,2,1,1,x,y,'',1.5,lx,ly,'','grid',0,1,'','');
y = ones(1,nData)*b2;
completeSubPlot(4,2,1,1,x,y,'--k',1.5,lx,ly,'','grid',0,1,'b2','');