package Aquisition;

import Control.PID;
import Sensors.RGB;
import USB.USBComm;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.nxt.SensorPort;
import lejos.util.Delay;

public class AquisitionUSB {

	public static final int REF = 20;
	public static final double KP = 0.5;
	public static final double KI = 0;
	public static final double KD = 0;
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";

	public static void main(String[] args) {
		NXTMotor motorCte = new NXTMotor(MotorPort.A); // Objeto para o motor de vel cte
		NXTMotor motorControl = new NXTMotor(MotorPort.B); // Objeto para o motor de vel vari�vel
		RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		USBComm usb = new USBComm(); // Objeto para comunica��o USB
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double u; // A��o de controle
		boolean firstLoop = true; // Informa se estamos no primeiro loop
		long time = 0; // Tempo de in�cio da simula��o
		long pastTime = 0; // Tempo total de simula��o
		long loopTime = 0; // Tempo do in�cio do loop atual
		String usbMsg; // Mensagem enviada ao PC atrav�s do USB

		// Esta fun��o � iniciada e aguarda at� que o computador estabele�a com o CLP a
		// comunica��o USB.
		usb.connect();

		LCD.clear();
		LCD.drawString("Pressione para \niniciar a \nsimulacao...", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0) {

		}

		LCD.clear();
		LCD.drawString("Simulacao \niniciada !", 0, 0);

		// Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		// pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		// motores) seja analisada separadamente.
		motorCte.setPower(50);
		while (Button.readButtons() == 0) {
			loopTime = System.currentTimeMillis();

			rbgValue = rgb.getSensor();
			u = pid.compute(rbgValue);
			motorControl.setPower((int) Math.round(u));

			if (firstLoop) {
				time = System.currentTimeMillis();
				firstLoop = false;
			}
			pastTime = System.currentTimeMillis() - time;
			usbMsg = u + RGB_MSG + rbgValue + TIME_MSG + pastTime;
			usb.send(usbMsg);

			Delay.msDelay(50 - (System.currentTimeMillis() - loopTime));
		}
		motorCte.stop();
		motorControl.stop();
		usbMsg = "*";
		usb.send(usbMsg);
		Delay.msDelay(300);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
