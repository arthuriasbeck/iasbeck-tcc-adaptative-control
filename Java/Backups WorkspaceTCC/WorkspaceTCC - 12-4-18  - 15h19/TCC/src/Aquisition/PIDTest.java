package Aquisition;

import Control.PID;
import Sensors.RGB;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.nxt.SensorPort;
import lejos.util.Delay;

public class PIDTest {

	public static final int REF = 400;
	public static final double KP = 0.2;
	public static final double KI = 0;
	public static final double KD = 0;
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";

	public static void main(String[] args) {
		NXTMotor motorCte = new NXTMotor(MotorPort.A); // Objeto para o motor de vel cte
		NXTMotor motorControl = new NXTMotor(MotorPort.B); // Objeto para o motor de vel vari�vel
		RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double u; // A��o de controle
		long loopTime = 0; // Tempo do in�cio do loop atual

		LCD.clear();
		LCD.drawString("Pressione para \niniciar a \nsimulacao...", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0) {

		}

		LCD.clear();
		LCD.drawString("Simulacao \niniciada !", 0, 0);

		// Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		// pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		// motores) seja analisada separadamente.
		motorCte.setPower(50);
		while (Button.readButtons() == 0) {
			loopTime = System.currentTimeMillis();
			rbgValue = rgb.getSensor();
			u = pid.compute(rbgValue);
			motorControl.setPower((int) Math.round(u));
			Delay.msDelay(50 - (System.currentTimeMillis() - loopTime));
		}
		motorCte.stop();
		motorControl.stop();
		Delay.msDelay(300);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
