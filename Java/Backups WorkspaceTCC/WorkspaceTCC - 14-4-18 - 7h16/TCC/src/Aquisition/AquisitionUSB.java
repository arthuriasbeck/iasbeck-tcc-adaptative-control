package Aquisition;

import Control.PID;
import Motors.NewMotor;
import Sensors.RGB;
import USB.USBComm;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.util.Delay;

public class AquisitionUSB {

	public static final boolean USB_COMM = true; // Coloque false para desativar a comunica��o USB
	public static final int REF = 420;
	public static final double KP = 0.14;
	public static final double KI = 0.005;
	public static final double KD = 0;
	public static final int MIN_POWER = 40;
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";
	public static final int NOISE_TIME = 50;

	public static void main(String[] args) {
		NewMotor motorA = new NewMotor(MotorPort.A, true);
		NewMotor motorB = new NewMotor(MotorPort.B, true);
		RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		USBComm usb = new USBComm(); // Objeto para comunica��o USB
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double u; // A��o de controle
		boolean firstLoop = true; // Informa se estamos no primeiro loop
		long time = 0; // Tempo de in�cio da simula��o
		long pastTime = 0; // Tempo total de simula��o
		long loopTime = 0; // Tempo do in�cio do loop atual
		String usbMsg; // Mensagem enviada ao PC atrav�s do USB
		int controlPower = 0; // A��o de controle sobre o motor
		long timeLastNoise = System.currentTimeMillis();
		long timeToNoise = 5000;
		long timeStartNoise = 0;
		boolean noising = false;
		int noise = 0;

		if (USB_COMM) {
			// Esta fun��o � iniciada e aguarda at� que o computador estabele�a com o CLP a
			// comunica��o USB.
			usb.connect();
		}

		LCD.clear();
		LCD.drawString("Pressione para \niniciar a \nsimulacao...", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0) {

		}

		LCD.clear();
		LCD.drawString("Simulacao \niniciada !", 0, 0);

		/*
		 * Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		 * pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		 * motores) seja analisada separadamente.
		 */
		motorB.setPower(MIN_POWER);
		motorB.forward();

		/*
		 * A ideia aqui � que a cada n segundos, sendo n um n�mero aleat�rio que vai de
		 * 1 a 5, uma perturba��o � adicionada na entrada da planta, ou seja, na
		 * pot�ncia do motor controlado. A perturba��o � um valor alet�rio que vai de 10
		 * a 40.
		 */
		while (Button.readButtons() == 0) {
			loopTime = System.currentTimeMillis();
			/*
			 * Controle do seguidor
			 */
			rbgValue = rgb.getSensor();
			u = pid.compute(rbgValue);
			/*
			 * Aqui o disturbio � inserido a cada timeToNoise segundos e a cada vez que isso
			 * acontece, o tempo para a inser��o do pr�ximo ru�do � recalculado.
			 */
			if (System.currentTimeMillis() - timeLastNoise > timeToNoise || noising) {
				if (!noising) {
					noising = true;
					Sound.playTone(1000, NOISE_TIME);
					timeStartNoise = System.currentTimeMillis(); // Tempo de in�cio do ru�do
					timeToNoise = Math.round(Math.random() * 3000 + 3000); // Tempo para o pr�ximo ru�do
					noise = (int) Math.round(Math.random() * 30 + 10); // Amplitude do ru�do
					LCD.clear();
					LCD.drawString("time=" + timeToNoise, 0, 0);
					LCD.drawString("noise = " + noise, 0, 1);
				}
				/*
				 * O ru�do cessa depois que se passam NOISE_TIME ms desde que ele come�ou a
				 * atuar (timeStartNoise).
				 */
				if (System.currentTimeMillis() - timeStartNoise > NOISE_TIME) {
					noising = false;
					timeLastNoise = System.currentTimeMillis();
				}
				// controlPower = MIN_POWER - (int) Math.round(u);
				// controlPower -= noise;
				controlPower = 0;
			} else {
				controlPower = MIN_POWER - (int) Math.round(u);
			}
			motorA.setPower(controlPower);
			LCD.clear(1);
			LCD.drawString("" + u, 0, 1);
			motorA.forward();
			/*
			 * Envio da mensagem
			 */
			if (USB_COMM) {
				if (firstLoop) {
					time = System.currentTimeMillis();
					firstLoop = false;
				}
				pastTime = System.currentTimeMillis() - time;

				usbMsg = controlPower + RGB_MSG + rbgValue + TIME_MSG + pastTime;
				usb.send(usbMsg);
			}

			Delay.msDelay(50 - (System.currentTimeMillis() - loopTime));
		}
		motorA.stop();
		motorB.stop();
		if (USB_COMM) {
			usbMsg = "*";
			usb.send(usbMsg);
		}
		Delay.msDelay(300);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
