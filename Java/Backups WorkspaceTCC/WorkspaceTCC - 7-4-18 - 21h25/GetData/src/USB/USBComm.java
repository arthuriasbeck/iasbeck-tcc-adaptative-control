package USB;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import lejos.pc.comm.NXTConnector;

public class USBComm {

	USBComm(String fileName){
		try{
			f = new File(fileName);
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
		}
		catch(IOException e){
			System.out.println("Problemas com arquivo");
		}
	}
	
	public static DataOutputStream outData;
	public static DataInputStream inData;
	public static NXTConnector link;
	public File f;
	public FileWriter w;
	public BufferedWriter bw;
	public boolean firstLoop = true;
	public long time = 0;
	
	public void send(double value){
		try {
			outData.writeFloat((float)value);
			outData.flush();
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}
	
	public void send(int value){
		try {
			outData.writeInt(value);
			outData.flush();
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}
	
	public double receiveDouble(){
		double read = 0;
		try {
			read = inData.readFloat();
			if (read == -1) {
				return read;
			}
		} catch (IOException e) {
			System.out.println("Problema no Receive");
		}
		System.out.println(read);
		
		try {
			bw.write(""+read);
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			System.out.println("Problemas com arquivo");
		}
		return read;
	}
	
	public int receiveInt(){
		int read = 0;
		try {
			read = inData.readInt();
		} catch (IOException e) {
			System.out.println("Problema no Receive");
		}
		System.out.println(read);
		
		try {
			bw.write(""+read);
			bw.write("\t");
			if(firstLoop){
				time = System.currentTimeMillis();
				firstLoop = false;
			}
			bw.write(""+(System.currentTimeMillis() - time));
			bw.newLine();
			bw.flush();
		} catch (IOException e) {
			System.out.println("Problemas com arquivo");
		}
		return read;
	}
	
	public void connect(){
		link = new NXTConnector();
	    
	     if (!link.connectTo("usb://"))
	     {
	        System.out.println("\nNo NXT find using USB");
	        }
	     
	     outData = new DataOutputStream(link.getOutputStream());
	     inData = new DataInputStream(link.getInputStream());
	     System.out.println("\nNXT is Connected");
	}
	
	/**
	 * Fun��o utilizada no teste da classe USBComm.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		double readValue = 0;
		USBComm usbComm = new USBComm("y.txt");
		usbComm.connect();
		while(readValue != -1.0) {
			readValue = usbComm.receiveDouble();
		}
	}
}
