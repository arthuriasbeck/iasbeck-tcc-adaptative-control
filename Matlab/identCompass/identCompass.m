% Este c�digo � respons�vel por tratar os dados obtidos a partir das
% simula��es realizadas para obten��o da planta do rob� seguidor que
% utiliza b�ssola. Aqui � realizada uma estima��o para que seja poss�vel
% obter os par�metros da planta que descrevem o funcionamento do seguidor e
% em seguida � realizada uma valida��o dos dados obtidos atrav�s do ident.

%% Obten��o dos dados referentes � experimenta��o com a b�ssola
clc;
clear;
close all;

F = '';

addpath('../libraries/PlotData');

% Upload dos dados
data = load('../../Java/WorkspaceTCC/PC/results/27 de Junho de 2018 - 16h41.txt');
input = data(:,1);
compass = data(:,2);
time = data(:,3);
timeS = time/1000;

% Plot dos dados de estima��o
x = 'Tempo (s)';
y = 'Entrada';
t = 'Entrada do sistema';
f = 'Dados para estima��o';
completeSubPlot(1,2,1,1,timeS,input,'',1.5,x,y,t,'grid',0,1,'',F);
y = 'Sa�da(�)';
t = 'Sa�da do sistema';
completeSubPlot(1,2,1,2,timeS,compass,'',1.5,x,y,t,'grid',0,1,f,F);

%% Estima��o da planta com m�nimos quadrados

% Caracter�sticas do modelo
nPoles = 2;
nZeros = 2;
uCurrentBool = 0;

% Inicialia��o de vari�veis para armazenar dados da execu��o
arrayErr = zeros(1,1);
arrayY = zeros(1,1);
arrayMod = zeros(1,1);

% Inicializa��o das vari�veis utilizadas na estima��o da planta
uCurrent = 0;
yCurrent = 0;
y = zeros(1,nPoles);
u = zeros(1,nZeros);
if uCurrentBool == 1
    fi = [-y uCurrent u];
else
    fi = [-y u];
end

parNum = length(fi);
P = rand(parNum);
teta = rand(parNum,1);

% La�o para obten��o dos par�metros da planta
for i = 1:length(input)
    % Obten��o da sa�da real do processo
    yCurrent = compass(i);
    uCurrent = input(i);
    
    % Atualiza��o dos par�metros do modelo
    if uCurrentBool == 1
        fi = [-y uCurrent u]';
    else
        fi = [-y u]';
    end
    
    P = P - P*fi*(1/(1 + fi'*P*fi))*fi'*P;
    K = P*fi;
    yModel = fi'*teta;
    err = yCurrent - yModel;
    teta = teta + K*err;
    
    % Armazenando informa��es sobre a execu��o
    arrayY(i) = yCurrent;
    arrayMod(i) = yModel;    
    arrayErr(i) = err;

    % Atualia��o dos vetores de entrada e sa�da
    shift = 1;
    
    uShift = zeros(size(u));
    uShift(1+shift:end) = u(1:end-shift);
    u = uShift;
    u(1) = uCurrent;
    
    yShift = zeros(size(y));
    yShift(1+shift:end) = y(1:end-shift);
    y = yShift;
    y(1) = yCurrent;
end

% Mostra um gr�fico para compararmos a sa�da real e a sa�da do modelo, e um
% para avaliarmos o erro (diferen�a entre a sa�da real e a sa�da
% do modelo).
x = 'Tempo (s)';
y = 'Sa�da(�)';
t = 'Sa�da do sistema';
f = 'Resultados da estima��o';
completeSubPlot(2,2,1,1,timeS,arrayMod,'',1.5,x,y,t,'grid',0,1,'',F);
completeSubPlot(2,2,1,1,timeS,arrayY,'',1.5,x,y,t,'grid',0,1,'',F);
legend('Sa�da estimada','Sa�da real');
t = 'Erro de estima��o';
y = 'Erro(�)';
completeSubPlot(2,2,1,2,timeS,arrayErr,'k',1.5,x,y,t,'grid',0,1,f,F);

display(teta);

%% Valida��o da planta obtida

% Upload dos dados
valData = load('../../Java/WorkspaceTCC/PC/results/27 de Junho de 2018 - 16h42.txt');

inputVal = valData(:,1);
compassVal = valData(:,2);
timeVal = valData(:,3);
timeS = timeVal/1000;

% Plot dos dados de valida��o
x = 'Tempo (s)';
y = 'Entrada';
t = 'Entrada do sistema';
f = 'Dados para valida��o';
completeSubPlot(3,2,1,1,timeS,inputVal,'',1.5,x,y,t,'grid',0,1,'',F);
y = 'Sa�da(�)';
t = 'Sa�da do sistema';
completeSubPlot(3,2,1,2,timeS,compassVal,'',1.5,x,y,t,'grid',0,1,f,F);

%%
% ESTA SE��O DO C�DIGO � EXECUTADA SOMENTE DEPOIS QUE A VALIDA��O � FEITA
% NO IDENT E EXPORTADA PARA UMA FIGURA 

% figure(4);
% grid on;
% axis([0 10 105 165]);
% set(findall(gca, 'Type', 'Line'),'LineWidth',1.5);
% title('Sa�da do sistema (88,9%)');
% xlabel('Tempo (s)');
% ylabel('Amplitude');
% legend('sa�da estimada','sa�da real');
% print('results\Resultado da valida��o', '-dpng');