package Aquisition;

import Control.PID;
import USB.USBComm;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.NXTMotor;
import lejos.util.Delay;

public class AquisitionUSB {

	public static final int REF = 20;
	public static final double KP = 0.5;
	public static final double KI = 0;
	public static final double KD = 0;
	public static final String TIME_MSG = "A";
	public static final String RGB_MSG = "B";

	public static void main(String[] args) {
		NXTMotor motorCte = new NXTMotor(MotorPort.A); // Objeto para o motor de vel cte
		NXTMotor motorControl = new NXTMotor(MotorPort.B); // Objeto para o motor de vel vari�vel
		// RGB rgb = new RGB(SensorPort.S1); // Objeto para o sensor RGB
		PID pid = new PID(REF, KP, KI, KD); // Objeto PID
		USBComm usb = new USBComm(); // Objeto para comunica��o USB
		double rbgValue = 0; // Valor do sensor RGB a cada itera��o
		double u; // A��o de controle
		boolean firstLoop = true;
		long time = 0;
		long pastTime;
		String usbMsg;

		usb.connect();

		// Um dos motores permanece constante em quanto o outro sobre varia��es dadas
		// pelo PID. Isso � realizado para que cada uma das entradas (pot�ncia dos
		// motores) seja analisada separadamente.
		motorCte.setPower(50);
		// while (Button.readButtons() == 0) {
		for (int i = 0; i < 100; i++) {
			// rbgValue = rgb.getSensor();
			rbgValue = Math.random() * 1023;
			u = pid.compute(rbgValue);
			motorControl.setPower((int) Math.round(u));

			if (firstLoop) {
				time = System.currentTimeMillis();
				firstLoop = false;
			}
			pastTime = System.currentTimeMillis() - time;
			usbMsg = u + RGB_MSG + rbgValue + TIME_MSG + pastTime;
			usb.send(usbMsg);

			/*
			 * TODO: Colocar no lugar desse delay uma l�gica que fa�a com que o la�o dure
			 * sempre 50 ms. Se a mensagem for enviada e o PID rodar em menos de 50 ms, eu
			 * espero at� 50 ms. Se demorar mais n�o tem tanto problema. � bom controlar o
			 * tempo desse la�o por causa do tempo de amostragem do sistema.
			 */
			Delay.msDelay(50);
		}
		motorCte.stop();
		motorControl.stop();
		usbMsg = "*";
		usb.send(usbMsg);
		Delay.msDelay(300);
		LCD.clear();
		LCD.drawString("Fim da simulacao \nPressione para \ncontinuar...", 0, 0);
		Button.waitForAnyPress();
	}

}
