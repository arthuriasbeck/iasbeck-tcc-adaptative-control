package control;

import java.util.ArrayList;

import communication.SendData;
import control.STR;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.MotorPort;
import lejos.nxt.SensorPort;
import lejos.util.Delay;
import motors.NewMotor;
import sensors.Compass;
import sensors.Sensor;

/**
 * Esta classe � uma tradu��o direta dos c�digos em Matlab que implementam o
 * controlador STR. Aqui � utilizada a classe STR, que possui um m�todo
 * (compute) respons�vel por realizar a estima��o e a a��o de controle.
 * 
 * @author Arthur Iasbeck
 *
 */

// TODO: Criar uma fun��o pra setar limites da a��o de controle
// TODO: Criar pasta results no RcvPack automaticamente caso ela n�o exista
// TODO: Receber como par�metro a amplitude da varia��o da refer�ncia no
// calibrate e no controlTest
// TODO: Receber como par�metro do calibrate o nome do arquivo em que quero
// salvar os resultados e no construtor o nome do arquivo de onde quero carregar
// os resultados.

public class STRControl {

	// Vari�veis para controle de execu��o (setadas externamente)
	private boolean motor = true;
	private boolean usb = false;

	// Par�metros do modelo
	private double ym;

	// Vari�veis utilizadas na simula��o da planta
	private double y = 0;
	private double u = 0;

	// Vari�veis utilizadas no controle do sistema
	private STR strControl = new STR();
	private NewMotor motorA;
	private NewMotor motorB;
	private double time;
	private double initialTime;
	private double lastRefTime = 0;
	private Sensor sensor;

	// Vari�veis utilizadas na simula��o do modelo
	public double uc = 0;
	private double ucMax = 0;
	private double ucMin = 0;

	// Vari�veis para envio dos dados da execu��o via USB
	private SendData sendData = null;
	private ArrayList<Double> msg = new ArrayList<Double>();

	public STRControl(Sensor sensor, NewMotor motorA, NewMotor motorB) {
		this.sensor = sensor;
		this.motorA = motorA;
		this.motorB = motorB;
	}

	public void sendUSB(boolean usb) {
		this.usb = usb;
	}

	private double firstSensorValue() {
		double sensorValue = 0;
		for (int i = 0; i < 10; i++) {
			sensorValue += sensor.getSensor();
			Delay.msDelay(50);
		}
		sensorValue /= 10;
		return sensorValue;
	}

	public void simulation(long refTime, long simTime, boolean calibrate) {
		// Inicializando objetos utilizados durante a execu��o
		if (usb) {
			sendData = new SendData();
		}
		strControl.setEstimation(calibrate);

		// Aguardando entrada do usu�rio para executar
		LCD.clear();
		LCD.drawString("Press to start", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0);
		LCD.clear();
		uc = 0;
		ucMax = firstSensorValue() + 40;
		ucMin = firstSensorValue() - 40;

		// La�o de execu��o
		time = System.currentTimeMillis();
		initialTime = System.currentTimeMillis();
		while (Button.readButtons() == 0 && (System.currentTimeMillis() - initialTime) < simTime) {
			// Computando sa�da da planta
			y = sensor.getSensor();

			// Alterando a refer�ncia
			if (System.currentTimeMillis() - lastRefTime > refTime) {
				lastRefTime = System.currentTimeMillis();
				if (uc == 0)
					uc = ucMax;
				else if (uc == ucMax)
					uc = ucMin;
				else if (uc == ucMin)
					uc = ucMax;
			}

			// Computando a entrada e atuando no sistema
			u = strControl.compute(y, uc);
			if (motor) {
				motorA.setPower((int) Math.round(40 + u));
				motorB.setPower((int) Math.round(40 - u));
				motorA.forward();
				motorB.forward();
			}

			// Obtendo sa�da do modelo
			ym = strControl.getModelOutput();

			// Contagem do tempo de simula��o
			time = System.currentTimeMillis();

			// Enviando os dados da execu��o via USB
			if (usb) {
				msg.clear();
				msg.add(y);
				msg.add(uc);
				msg.add(ym);
				msg.add(u);
				msg.add(time);
				sendData.send(msg);
			}
		}

		// Finalizando calibra��o
		motorA.stop();
		motorB.stop();
		strControl.save();
		if (usb) {
			sendData.end();
		}
	}

	public void calibrate(long refTime, long simTime, double Mp, double ta, double T, boolean reset) {
		if (reset) {
			strControl.reset();
		}
		strControl.setControl(Mp, ta, T);
		simulation(refTime, simTime, true);
		LCD.clear();
		LCD.drawString("Calibrate done", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0);
	}

	public void controlTest(long refTime, long simTime) {
		simulation(refTime, simTime, false);
		LCD.clear();
		LCD.drawString("Test done", 0, 0);
		Button.waitForAnyPress();
		while (Button.readButtons() != 0)
			;
	}

	public double control(double y, double uc) {
		return strControl.compute(y, uc);
	}

	public static void main(String args[]) {
		Compass compass = new Compass(SensorPort.S2);
		NewMotor motorA = new NewMotor(MotorPort.A, true);
		NewMotor motorB = new NewMotor(MotorPort.B, true);
		STRControl strControl = new STRControl(compass, motorA, motorB);

		double Mp = 0.1;
		double ta = 1;
		double T = 0.05;
		long refTime = 4000;
		long simTime = refTime * 3;
		/* Envio USB */
		// strControl.sendUSB(false);

		/* Calibra��o */
		strControl.calibrate(refTime, simTime, Mp, ta, T, false);

		/* Controle */
		// double y, u, uc;
		// uc = 20; // Refer�ncia para a b�ssola
		// LCD.clear();
		// while (true) {
		// y = compass.getSensor();
		// u = strControl.control(y, uc);
		// LCD.drawString("u = " + u, 0, 0);
		// LCD.drawString("y = " + y, 0, 1);
		// motorA.setPower((int) Math.round(40 + u));
		// motorB.setPower((int) Math.round(40 - u));
		// motorA.forward();
		// motorB.forward();
		// }
	}
}