package communication;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;
import lejos.nxt.comm.USB;
import lejos.nxt.comm.USBConnection;
import lejos.util.Delay;

public class USBComm {

	public USBConnection USBLink;
	public DataOutputStream dataOut;
	public DataInputStream dataIn;

	public void connect() {
		LCD.clear();
		LCD.drawString("Listening", 0, 0);
		USBLink = USB.waitForConnection();
		dataOut = USBLink.openDataOutputStream();
		dataIn = USBLink.openDataInputStream();
		LCD.clear();
		LCD.drawString("Done", 0, 0);
		Delay.msDelay(1000);
		LCD.clear();
	}

	public void send(String msg) {
		/*
		 * O caractere 'X' deve ser adicionado ao fim de todas as mensagens pois �
		 * atrav�s deste caractere que o recebimento da String � finalizado. Uma vez que
		 * s� � poss�vel receber caracteres, o computador realiza a leitura de cada um
		 * dos caracteres da mensagem at� encontrar o caracter 'X'. Neste momento, ele
		 * sabe que a mensagem acabou.
		 */
		msg = msg + "x";
		try {
			dataOut.writeChars(msg);
			dataOut.flush();
		} catch (IOException e) {
			LCD.clear();
			LCD.drawString("Problema no Send", 0, 0);
		}
		// Delay para impedir que ocorram erros durante o envio das mensagens.
		Delay.msDelay(10);
	}

	// Fun��o utilizada no teste da classe USBComm
	public static void main(String args[]) {
		USBComm usbComm = new USBComm();
		String writeValue;
		usbComm.connect();
		for (int i = 0; i < 10; i++) {
			writeValue = "" + Math.random() * 20;
			usbComm.send(writeValue);
		}
		usbComm.send("*");
	}

}
