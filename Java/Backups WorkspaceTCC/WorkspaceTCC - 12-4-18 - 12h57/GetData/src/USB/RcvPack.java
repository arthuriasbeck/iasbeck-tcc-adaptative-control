package USB;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class RcvPack {

	public static final char TIME_MSG = 'A';
	public static final char RGB_MSG = 'B';

	public static void main(String args[]) {
		
		String fileName = "testRcvPack.txt";
		File f = null;
		FileWriter w = null;
		BufferedWriter bw = null;
		
		try {
			f = new File(fileName);
			w = new FileWriter(f);
			bw = new BufferedWriter(w);
		} catch (IOException e) {
			System.out.println("Erro na cria��o do arquivo");
		}
		
		String readValue = "";
		String u = "";
		String rgbValue = "";
		String time = "";
		int timeIndex = -1;
		int rbgIndex = -1;

		USBCommPC usbComm = new USBCommPC("rcvUSB.txt");
		usbComm.connect();

		while (true) {
			readValue = usbComm.receiveString();
			System.out.println("\nreadValue = " + readValue);
			if (readValue.compareTo("*") == 0) {
				break;
			}
			
			rbgIndex = readValue.indexOf(RGB_MSG);
			timeIndex = readValue.indexOf(TIME_MSG);
			u = readValue.substring(0, rbgIndex);
			rgbValue = readValue.substring(rbgIndex + 1, timeIndex);
			time = readValue.substring(timeIndex + 1);
			System.out.println("u = " + u);
			System.out.println("rgbValue = " + rgbValue);
			System.out.println("time = " + time);
			
			try {
				bw.write(u + "\t" + rgbValue + "\t" + time);
				bw.newLine();
				bw.flush();
			} catch (IOException e) {
				System.out.println("Erro na escrita do arquivo");
			}
		}
		System.out.println("Fim da execu��o");
		try {
			bw.close();
		} catch (IOException e) {
			System.out.println("Erro no fechamento do arquivo");
		}
		usbComm.close();
	}
}
