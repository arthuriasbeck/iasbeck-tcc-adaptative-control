% Este c�digo tem objetivo de mostrar a import�ncia de um controlador
% adaptativo. Aqui o sistema a ser controlado tem seus par�metros
% modificados na metade da simula��o e partir da� o controlador adaptativo
% deve se adaptar �s mudan�as sofridas pelo sistema.

addpath('libraries');
addpath('../libraries/PlotData')

clear; clc; close all;

% Par�metros para execu��o
T = 0.05;       % Tempo de amostragem
tSim = 20;      % Tempo de simula��o
tInput = 2;     % Tempo para que ocorra uma mudan�a na entrada do sistema
ucAmp = 90;     % Determina a amplitude da entrada degrau
ucSig = -1;     % Determina se o degrau ser� positivo ou negativo

% Par�metros do modelo
am1 = -1.6302;
am2 = 0.6700;
a0 = 0;

% Inicializa��o de vari�veis para armazenamento de dados
uArray = zeros(1,1);    % Vetor que armazena os valores de entrada
ucArray = zeros(1,1);    % Vetor que armazena os valores de refer�ncia
yArray = zeros(1,1);    % Vetor que armazena os valores de sa�da
yeArray = zeros(1,1);   % Vetor que armazena os valores de estima��o
ymArray = zeros(1,1);   % Vetor que armazena os valores do modelo
a1Array = zeros(1,1);   % Vetor que armazena os valores de a1
a2Array = zeros(1,1);   % Vetor que armazena os valores de a2
b0Array = zeros(1,1);   % Vetor que armazena os valores de b0
b1Array = zeros(1,1);   % Vetor que armazena os valores de b1

% Inicializa��o de vari�veis utilizadas na simula��o da planta
y = 0;      % y(t)
y_1 = 0;    % y(t-1)
y_2 = 0;    % y(t-2)
u = 0;      % u(t)
u_1 = 0;    % u(t-1)
u_2 = 0;    % u(t-2)

% Inicializa��o das vari�veis utilizadas na simula��o do modelo
ym = 0;     % ym(t)
ym_1 = 0;   % ym(t-1)
ym_2 = 0;   % ym(t-2)
uc = 0;     % uc(t)
uc_1 = 0;   % uc(t-1)
uc_2 = 0;   % uc(t-2)

% Inicializa��o das vari�veis utilizadas na estima��o
ye = 0;                     % Sa�da estimada pelo m�nimos quadrados
fi = [-y_1 -y_2 u_1 u_2];   % Vetor de vari�veis conhecidas
parNum = length(fi);        % Quantidade de par�metros a serem determinados
P = 5*rand(parNum);         % Matriz de corre��o
teta = rand(parNum,1);      % Vetor de par�metros

% Inicializa��o dos par�metros da planta
a1 = 0;
a2 = 0;
b0 = 0;
b1 = 0;

% Vari�veis para controle de execu��o
numLoops = tSim/T;

for loops = 1:numLoops
    % Computando sa�da da planta
    if loops < numLoops/2
        y = 1.0002*y_1 + 0.0008*y_2 - 0.0244*u_1 + 0.1503*u_2;
    end
    if loops >= numLoops/2
        if loops == numLoops/2
            fi = [-y_1 -y_2 u_1 u_2];   % Vetor de vari�veis conhecidas
            parNum = length(fi);        % Quantidade de par�metros a serem determinados
            P = eye(parNum)*5;          % Matriz de corre��o
            teta = rand(parNum,1);      % Vetor de par�metros
        end
        y = 1.0002*y_1 + 0.0008*y_2 + 1*u_1 + 0.1503*u_2;
    end
    
    % Atualiza��o dos par�metros do modelo
    fi = [-y_1 -y_2 u_1 u_2]';
    P = P - P*fi*(1/(1 + fi'*P*fi))*fi'*P;
    K = P*fi;
    ye = fi'*teta;
    errEst = y - ye;
    teta = teta + K*errEst;
    
    % Atualizando par�metros da planta
    
    % Descomente abaixo para obter um controlador adaptativo
    
        a1 = teta(1);
        a2 = teta(2);
        b0 = teta(3);
        b1 = teta(4);
    
    % Descomente abaixo para obter um controlador de ganhos constantes
    
%     a1 = -1.0002;
%     a2 = -0.0008;
%     b0 = -0.0244;
%     b1 = 0.1503;
    
    % Atualizando par�metros do controlador
    [s0, s1, r1, beta] = getControlPar(a0, a1, a2, b0, b1, am1, am2);
    if loops < numLoops/2
        s0 = 0.3222;
        s1 = -0.0031;
        r1 = -0.6221;
        beta = 0.3161;
    end
    
    % Computando sa�da do modelo
    ym = -am1*ym_1 - am2*ym_2 + beta*b0*uc_1 + beta*b1*uc_2;
    
    % Computando as entradas
    ucSig = changeRef(loops, tInput, T, ucSig);
    uc = ucAmp*ucSig;
    u = -r1*u_1 + beta*uc + beta*a0*uc_1 - s0*y - s1*y_1;
    % u = correctInput(u);
    
    % Armazenando dados de execu��o
    uArray(loops) = u;
    ucArray(loops) = uc;
    yArray(loops) = y;
    yeArray(loops) = ye;
    ymArray(loops) = ym;
    a1Array(loops) = a1;
    a2Array(loops) = a2;
    b0Array(loops) = b0;
    b1Array(loops) = b1;
    
    % Atualizando vari�veis que armanezam dados de itera��es passadas
    y_2 = y_1;
    y_1 = y;
    u_2 = u_1;
    u_1 = u;
    ym_2 = ym_1;
    ym_1 = ym;
    uc_2 = uc_1;
    uc_1 = uc;
end

% Mostrando os resultados da simula��o
U = uArray;
UC = ucArray;
Y = yArray;
YE = yeArray;
YM = ymArray;
A1 = a1Array;
A2 = a2Array;
B0 = b0Array;
B1 = b1Array;
TETA = [1 0.0008 1 0.1503];

plotResultsControl(numLoops, T, U, UC, Y, YE, YM, A1, A2, B0, B1, TETA);
