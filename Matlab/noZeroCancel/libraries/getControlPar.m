function [s0, s1, r1, beta] = getControlPar(a0, a1, a2, b0, b1, am1, am2)
    r1 = (am2 + am1*a0 - a2 - (b0/b1)*am2*a0 - (b1/b0)*(am1 - a1 + a0))/(a1 - (b0/b1)*a2 - (b1/b0));
    s0 = (am1 + a0 - a1 - r1)/b0;
    s1 = (am2*a0 - a2*r1)/b1;
    beta = (1 + am1 + am2)/(b0 + b1);
end