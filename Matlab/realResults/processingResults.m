% Este c�digo � respons�vel por apresentar de forma gr�fica os dados
% obtidos experimentalmente atrav�s da implementa��o de um STR que n�o
% realiza o cancelamento dos zeros da planta ao controle de trajet�ria do
% seguidor.

for sample = 1:3
    
    clearvars -except sample; clc; close all;
    addpath('../libraries/PlotData');
    dataNumber = num2str(sample);
    if strcmp(dataNumber,'1')
        gap = 107:162;
    end
    if strcmp(dataNumber,'2')
        gap = 83:123;
    end
    if strcmp(dataNumber,'3')
        gap = 92:138;
    end
    tickSize = 16;
    
    % Carregando os dados
    data = load(strcat(dataNumber,'.txt'));
    yArray = data(:,1);
    ucArray = data(:,2);
    ymArray = data(:,3);
    uArray = data(:,4);
    time = (data(:,5) - data(1,5))/1000;
    
    % Tratando os dados
    dt = zeros(1,1);
    for loop = 2:length(data)
        dt(loop) = time(loop) - time(loop-1);
    end
    
    sumTime = 0;
    newYmArray = zeros(1,1);
    newYArray = zeros(1,1);
    newUcArray = zeros(1,1);
    newUArray = zeros(1,1);
    newTime = zeros(1,1);
    for loop = 1:length(data)
        sumTime = sumTime + dt(loop);
        if sumTime > 0.05
            sumTime = 0;
            newYmArray(loop) = ymArray(loop);
            newYArray(loop) = yArray(loop);
            newUcArray(loop) = ucArray(loop);
            newUArray(loop) = uArray(loop);
            newTime(loop) = time(loop);
        else
            newYmArray(loop) = -999;
            newYArray(loop) = -999;
            newUcArray(loop) = -999;
            newUArray(loop) = -999;
            newTime(loop) = -999;
        end
    end
    
    newYmArray(newYmArray == -999) = [];
    newYArray(newYArray == -999) = [];
    newUcArray(newUcArray == -999) = [];
    newUArray(newUArray == -999) = [];
    newTime(newTime == -999) = [];
    
    yArray = newYArray;
    ucArray = newUcArray;
    ymArray = newYmArray;
    uArray = newUArray;
    time = newTime;
    
    % Mostrando a evolu��o da sa�da do sistema
    lx = 'Tempo (s)';
    ly = 'Sa�da (�)';
    t = 'Sa�da do processo x Sa�da do modelo';
    f = strcat('y_ym_uc','_',dataNumber);
    completePlotDigital(1,time,ucArray,'k',1.5,lx,ly,t,'grid',0,1,'','',1);
    completePlotDigital(1,time,ymArray,'r',1.5,lx,ly,t,'grid',0,1,'','',1);
    completePlotDigital(1,time,yArray,'b',1.5,lx,ly,t,'grid',0,1,'','',1);
    legend('Refer�ncia','Sa�da desejada','Sa�da real');
    set(gcf,'units','points','position',[20,100,900,400]);
    get(gca, 'XTick');
    set(gca, 'FontSize', tickSize);
    completePlotDigital(1,time,yArray,'b',1.5,lx,ly,t,'grid',0,1,f,'',1);
    
    % Mostrando a evolu��o da a��o de controle
    t = 'Entrada do processo (a��o de controle)';
    ly = 'Entrada';
    f = strcat('u','_',dataNumber);
    completePlotDigital(2,time,uArray,'b',1.5,lx,ly,t,'grid',0,1,'','',1);
    set(gcf,'units','points','position',[20,100,900,400]);
    get(gca, 'XTick');
    set(gca, 'FontSize', tickSize);
    sizeAxis = axis;
    sizeAxis(3) = -70; sizeAxis(4) = 70;
    axis(sizeAxis);
    print(strcat('results\',f),'-dpng');
    savefig(strcat('results\figs\',f));
    
    % Comparando a sa�da do sistema � sa�da do modelo para o controlador j�
    % ajustado
    t = 'Sa�da do processo x Sa�da do modelo';
    ly = 'Sa�da (�)';
    f = strcat('y_ym','_',dataNumber);
    ymArray = ymArray(gap);
    yArray = yArray(gap);
    time = time(gap);
    
    completePlotDigital(3,time,ymArray,'r',1.5,lx,ly,t,'grid',0,1,'','',1);
    completePlotDigital(3,time,yArray,'b',1.5,lx,ly,t,'grid',0,1,'','',1);
    lgd = legend('Sa�da desejada','Sa�da real');
    if(sample == 2)
        lgd.Position(2) = lgd.Position(2) - 0.15;
    else
        lgd.Position(2) = lgd.Position(2) - 0.1;
    end
    completePlotDigital(3,time,yArray,'b',1.5,lx,ly,t,'grid',0,1,f,'',1);
    
    % Calculando a diferen�a entre y e ym
    e = ymArray - yArray;
    e = abs(e);
    e = sum(e)/length(e);
    display(e);
    
end