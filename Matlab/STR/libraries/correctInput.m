function uCorrect = correctInput(u)
    uCorrect = u;
    if u > 60
        uCorrect = 60;
    end
    if u < -60
        uCorrect = -60;
    end
end