package USB;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import lejos.nxt.LCD;
import lejos.nxt.comm.USB;
import lejos.nxt.comm.USBConnection;
import lejos.util.Delay;

public class USBComm {

	public USBConnection USBLink;
	public DataOutputStream dataOut;
	public DataInputStream dataIn;

	public void connect() {
		LCD.drawString("Listening", 0, 0);
		USBLink = USB.waitForConnection();
		dataOut = USBLink.openDataOutputStream();
		dataIn = USBLink.openDataInputStream();
		LCD.clear();
		LCD.drawString("Done", 0, 0);
		Delay.msDelay(1000);
		LCD.clear();
	}

	public double receiveDouble() {
		double read = 0;
		try {
			read = dataIn.readFloat();
		} catch (IOException e) {
			System.out.println("Problema no Receive");
		}
		LCD.clear();
		LCD.drawString("" + read, 0, 0);
		return read;
	}

	public int receiveInt() {
		int read = 0;
		try {
			read = dataIn.readInt();
		} catch (IOException e) {
			System.out.println("Problema no Receive");
		}
		LCD.clear();
		LCD.drawString("" + read, 0, 0);
		return read;
	}

	public void send(String msg) {
		/*
		 * O caractere 'X' deve ser adicionado ao fim de todas as mensagens pois �
		 * atrav�s deste caractere que o recebimento da String � finalizado. Uma vez que
		 * s� � poss�vel receber caracteres, o computador realiza a leitura de cada um
		 * dos caracteres da mensagem at� encontrar o caracter 'X'. Neste momento, ele
		 * sabe que a mensagem acabou.
		 */
		msg = msg + "X";
		try {
			dataOut.writeChars(msg);
			dataOut.flush();
			LCD.clear();
			LCD.drawString("Enviado", 0, 0);
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}

	public void send(double value) {
		try {
			dataOut.writeFloat((float) value);
			dataOut.flush();
			LCD.clear();
			LCD.drawString("Enviado", 0, 0);
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}

	public void send(int value) {
		try {
			dataOut.writeInt(value);
			dataOut.flush();
		} catch (IOException e) {
			System.out.println("Problema no Send");
		}
	}

	public void testDouble() {
		double writeValue;
		USBComm usbComm = new USBComm();
		usbComm.connect();
		for (int i = 0; i < 10; i++) {
			writeValue = Math.random() * 20;
			usbComm.send(writeValue);

			// Se n�o houver nenhum delay, � poss�vel que ocorram erros durante o envio dos
			// dados.
			Delay.msDelay(100);
		}
		usbComm.send(-1.0);
	}

	public void testString() {
		String writeValue;
		USBComm usbComm = new USBComm();
		usbComm.connect();
		for (int i = 0; i < 10; i++) {
			writeValue = "" + Math.random() * 20;
			usbComm.send(writeValue);

			// Se n�o houver nenhum delay, � poss�vel que ocorram erros durante o envio dos
			// dados.
			Delay.msDelay(100);
		}
		usbComm.send("*");
	}

	/**
	 * Fun��o utilizada no teste da classe USBComm.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		/*
		 * Teste para o envio de um double
		 */
		// double writeValue;
		// USBComm usbComm = new USBComm();
		// usbComm.connect();
		// for (int i = 0; i < 10; i++) {
		// writeValue = i;
		// usbComm.send(writeValue);
		// // Se n�o houver nenhum delay, � poss�vel que ocorram erros durante o envio
		// dos
		// // dados.
		// Delay.msDelay(100);
		// }
		// usbComm.send(-1.0);

		/**
		 * Teste para o envio de uma string
		 */
		String writeValue;
		USBComm usbComm = new USBComm();
		usbComm.connect();
		for (int i = 0; i < 10; i++) {
			writeValue = "" + i;
			usbComm.send(writeValue);

			// Se n�o houver nenhum delay, � poss�vel que ocorram erros durante o envio
			// dos dados.
			Delay.msDelay(400);
		}
		usbComm.send("*X");
	}

}
