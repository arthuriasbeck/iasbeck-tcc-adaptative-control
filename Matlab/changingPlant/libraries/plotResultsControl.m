function plotResultsControl(numLoops, T, uArray, ucArray, yArray, yeArray, ymArray, a1Array, a2Array, b0Array, b1Array, realTeta)
    % Mostrando resultados da simula��o da planta
    time = (0:numLoops-1)*T;
    lX = 'Tempo(s)';
    lY = 'Entrada';
    file = 'Entrada do processo';
    title = 'Entrada do processo';
    completePlot(1,time,uArray,'r',1.5,lX,lY,title,'grid',0,1,file,'');

    title = 'Sa�da do processo';
    lY = 'Sa�da';
    file = 'Sa�da do processo';
    completePlot(2,time,ucArray,'k',1.5,lX,lY,title,'grid',0,1,'','');
    completePlot(2,time,yArray,'b',1.5,lX,lY,title,'grid',0,1,'','');
    legend('Refer�ncia','Sa�da');
    completePlot(2,time,yArray,'b',1.5,lX,lY,title,'grid',0,1,file,'');
    
    % Mostrando compara��o entre a sa�da do processo e do modelo
    title = 'Sa�da do processo x Sa�da do modelo';
    file = 'Compara��o entre a sa�da do processo e a sa�da do modelo';
    completePlotDigital(3,time,ymArray,'b',1.5,lX,lY,title,'grid',0,1,'','',0);
    completePlotDigital(3,time,ucArray,'k',1.5,lX,lY,title,'grid',0,1,'','',0);
    completePlotDigital(3,time,yArray,'r',1.5,lX,lY,title,'grid',0,1,'','',0);
    legend('Sa�da desejada','Refer�ncia','Sa�da real');
    completePlotDigital(3,time,yArray,'r',1.5,lX,lY,title,'grid',0,1,file,'',0);

    % Mostrando dados da estima��o
    title = 'Sa�da do processo x Sa�da estimada';
    file = 'Resultados da estima��o';
    completePlot(4,time,yArray,'b',1.5,lX,lY,title,'grid',0,1,'','');
    completePlot(4,time,yeArray,'k',1.5,lX,lY,title,'grid',0,1,file,'');
    legend('Sa�da do process','Sa�da estima��o');

    % Mostrando evolu��o dos par�metros ao longo do tempo
    % Par�metro a1
    lY = 'a1';
    a1Real = ones(1,numLoops)*(realTeta(1));
    a1Last = a1Array(end);
    title = strcat({'a1 = '}, num2str(a1Last),{' ('}, num2str(realTeta(1)),{ ')'});
    completeSubPlot(5,2,1,1,time,a1Array,'',1.5,lX,lY,title,'',0,0.07,'','');
    completeSubPlot(5,2,1,1,time,a1Real,'--k',1.5,lX,lY,title,'',0,0.07,'','');

    % Par�metro a2
    lY = 'a2';
    a2Real = ones(1,numLoops)*(realTeta(2));
    a2Last = a2Array(end);
    title = strcat({'a2 = '}, num2str(a2Last),{' ('}, num2str(realTeta(2)),{ ')'});
    file = 'Evolu��o dos par�metros a';
    completeSubPlot(5,2,1,2,time,a2Array,'',1.5,lX,lY,title,'',0,0.07,'','');
    completeSubPlot(5,2,1,2,time,a2Real,'--k',1.5,lX,lY,title,'',0,0.07,file,'');

    % Par�metro b0
    lY = 'b0';
    b0Real = ones(1,numLoops)*(realTeta(3));
    b0Last = b0Array(end);
    title = strcat({'b0 = '}, num2str(b0Last),{' ('}, num2str(realTeta(3)),{ ')'});
    completeSubPlot(6,2,1,1,time,b0Array,'',1.5,lX,lY,title,'',0,0.07,'','');
    completeSubPlot(6,2,1,1,time,b0Real,'--k',1.5,lX,lY,title,'',0,0.07,'','');

    % Par�metro b1
    lY = 'b1';
    b1Real = ones(1,numLoops)*(realTeta(4));
    b1Last = b1Array(end);
    title = strcat({'b1 = '}, num2str(b1Last),{' ('}, num2str(realTeta(4)),{ ')'});
    file = 'Evolu��o dos par�metros b';
    completeSubPlot(6,2,1,2,time,b1Array,'',1.5,lX,lY,title,'',0,0.07,'','');
    completeSubPlot(6,2,1,2,time,b1Real,'--k',1.5,lX,lY,title,'',0,0.07,file,'');
end